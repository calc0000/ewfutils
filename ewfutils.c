#include "ewfutils.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <memory.h>
#include <math.h>

#ifdef _MSC_VER
#pragma warning(disable:4996)
#endif

int streq(char* str1,char* str2);

#pragma region EWF_Hash
//MurmurHash3 translation to C from source at http://code.google.com/p/smhasher/source/browse/trunk/MurmurHash3.cpp
void    EWF_Hash_Murmur_x86_32  (const void* key,int len,unsigned int seed,void* out){
    const unsigned char* data=(const unsigned char*)key;
    int i=0;
    const int nblocks=len/4;
    unsigned int h1=seed;
    const unsigned int c1=0xcc9e2d51;
    const unsigned int c2=0x1b873593;
    unsigned int k1=0;
    const unsigned int* blocks=(const unsigned int*)(data+nblocks*4);
    const unsigned char* tail=(const unsigned char*)(data+nblocks*4);

    for(i=-nblocks;i;i++){
        unsigned int k1=blocks[i];
        k1*=c1;
        k1=(k1<<15)|(k1>>(32-15));
        k1*=c2;
        h1^=k1;
        h1=(h1<<13)|(h1>>(32-13));
        h1=h1*5+0xe6546b64;
    }

    switch(len&3){
    case 3: k1^=tail[2]<<16;
    case 2: k1^=tail[1]<<8;
    case 1: k1^=tail[0];
        k1*=c1;
        k1=(k1<<15)|(k1>>(32-15));
        k1*=c2;
        h1^=k1;
    }

    h1^=len;
    h1^=h1>>16;
    h1*=0x85ebca6b;
    h1^=h1>>13;
    h1*=0xc2b2ae35;
    h1^=h1>>16;

    *(unsigned int*)out=h1;
}
#pragma endregion

#pragma region EWF_MEMFILE

/*EWF_MEMFILE*/
const int   __EWF_MEMFILE_CHUNKSIZE=1024;
int mclose(EWF_MEMFILE* stream){
    if(stream->backend.use && stream->backend.close && stream->backend.control)
        stream->backend.close(stream->backend.closure);
    free(stream->buffer);
    free(stream);
    return 0;
}
EWF_MEMFILE* mopen(const char* filename,const char* mode){
    return mlink(mnew(),fopen(filename,mode));
}
EWF_MEMFILE* mnew(){
    EWF_MEMFILE* f=(EWF_MEMFILE*)malloc(sizeof(EWF_MEMFILE));
    memset(f,0,sizeof(EWF_MEMFILE));
    f->buffer=(unsigned char*)malloc(sizeof(unsigned char)*__EWF_MEMFILE_CHUNKSIZE);
    memset(f->buffer,0,sizeof(unsigned char)*__EWF_MEMFILE_CHUNKSIZE);
    f->size=0;
    f->capacity=__EWF_MEMFILE_CHUNKSIZE;
    return f;
}
size_t mread(void* dstbuf,size_t size,size_t count,EWF_MEMFILE* stream){
    size_t numRead=size*count;
    if(stream->backend.use && stream->backend.read)
        return stream->backend.read(dstbuf,size,count,stream->backend.closure);
    if(stream->readIndex+(int)numRead >= stream->size)
        numRead=stream->size-stream->readIndex;
    memcpy(dstbuf,(stream->buffer+sizeof(unsigned char)*stream->readIndex),numRead);
    stream->readIndex+=numRead;
    return numRead;
}
char* mgets (char* str,int num,EWF_MEMFILE* stream){
    //str is already allocated
    size_t charsToRead=0;
    size_t startingIndex=stream->readIndex;
    size_t endingIndex=0;
    if(stream->backend.use && stream->backend.gets)
        return stream->backend.gets(str,num,stream->backend.closure);
    //look ahead
    {
        size_t i=0;
        for(i=startingIndex;i<stream->size;i++){
            if(stream->buffer[i]=='\n')
                break;
        }
        if(i!=stream->size)
            i++;
        endingIndex=i;
    }
    charsToRead=endingIndex-startingIndex;
    if(charsToRead==0)
        return NULL;
    if(charsToRead>num)
        charsToRead=num;
    mread(str,sizeof(char),charsToRead,stream);
    return str;
}
int mprintf(EWF_MEMFILE* stream,char* format,...){
    va_list ap;
    char msg[1024];
    unsigned int num=0;
    memset(msg,'\0',1024);
    va_start(ap,format);
    num=vsnprintf(msg,1023,format,ap);
    va_end(ap);
    mwrite(msg,sizeof(char),num,stream); //kicks backend responsibility to mwrite
    return num;
}
size_t mwrite(const void* dstbuf,size_t size,size_t count,EWF_MEMFILE* stream){
    size_t numWritten=size*count;
    if(stream->backend.use && stream->backend.write)
        return stream->backend.write(dstbuf,size,count,stream->backend.closure);
    if(stream->writeIndex+(int)numWritten > stream->capacity){
        mresize(stream,(int)((stream->writeIndex+numWritten)/__EWF_MEMFILE_CHUNKSIZE+1));
    }
    memcpy((stream->buffer+sizeof(unsigned char)*stream->writeIndex),dstbuf,size*count);
    if(stream->writeIndex>=stream->size)
        stream->size+=numWritten;
    else
        if((int)numWritten<(stream->size-stream->writeIndex));
        else
            stream->size+=numWritten-(stream->size-stream->writeIndex);
    stream->writeIndex+=size*count;
    return numWritten;
}
size_t mtell(EWF_MEMFILE* stream){
    return mtellr(stream);
}
size_t mtellr(EWF_MEMFILE* stream){
    if(stream->backend.use && stream->backend.tell)
        return stream->backend.tell(stream->backend.closure);
    return stream->readIndex;
}
size_t mtellw(EWF_MEMFILE* stream){
    if(stream->backend.use && stream->backend.tell)
        return stream->backend.tell(stream->backend.closure);
    return stream->writeIndex;
}
int mseek(EWF_MEMFILE* stream,long int offset,int origin){
    return mseekr(stream,offset,origin);
}
int mseekr(EWF_MEMFILE* stream,long int offset,int origin){
    size_t opos=0;
    if(stream->backend.use && stream->backend.seek)
        return stream->backend.seek(stream->backend.closure,offset,origin);
    switch(origin){
        case MSEEK_SET:
            opos=0;
            break;
        case MSEEK_CUR:
            opos=stream->readIndex;
            break;
        case MSEEK_END:
            opos=stream->size;
            break;
    }
    if(opos+offset > stream->size)
        return -1;
    if(opos+offset < 0)
        return -2;
    stream->readIndex=opos+offset;
    return 0;
}
int mseekw(EWF_MEMFILE* stream,long int offset,int origin){
    size_t opos=0;
    if(stream->backend.use && stream->backend.seek)
        return stream->backend.seek(stream->backend.closure,offset,origin);
    switch(origin){
        case MSEEK_SET:
            opos=0;
            break;
        case MSEEK_CUR:
            opos=stream->writeIndex;
            break;
        case MSEEK_END:
            opos=stream->size;
            break;
    }
    if(opos+offset < 0)
        return -2;
    stream->writeIndex=opos+offset;
    return 0;
}
void mtrim(EWF_MEMFILE* stream){
    stream->buffer=realloc(stream->buffer,sizeof(unsigned char)*stream->size);
    stream->capacity=stream->size;
}
void mcap(EWF_MEMFILE* stream,long int capacity){
    stream->buffer=realloc(stream->buffer,sizeof(unsigned char)*capacity);
    if(stream->writeIndex>=capacity)
        stream->writeIndex=capacity-1;
    if(stream->readIndex>=capacity)
        stream->readIndex=capacity-1;
    stream->capacity=capacity;
    if(stream->size>capacity)
        stream->size=capacity;
}
unsigned char* mgetptr(EWF_MEMFILE* stream){
    return mgetptrw(stream);
}
unsigned char* mgetptrr(EWF_MEMFILE* stream){
    return stream->buffer+stream->readIndex;
}
unsigned char* mgetptrw(EWF_MEMFILE* stream){
    return stream->buffer+stream->writeIndex;
}
unsigned char* mresize(EWF_MEMFILE* stream,int numberOfTimes){
    if(stream->resizeFxn!=NULL)
        return stream->resizeFxn(stream,numberOfTimes);
    stream->buffer=realloc(stream->buffer,stream->capacity+(numberOfTimes*__EWF_MEMFILE_CHUNKSIZE));
    stream->capacity=stream->capacity+(numberOfTimes*__EWF_MEMFILE_CHUNKSIZE);
    return stream->buffer;
}
EWF_MEMFILE* mlink(EWF_MEMFILE* mf,FILE* f){
    if(f){
        mf->backend.closure=f;
        mf->backend.use=1;
        mf->backend.read=fread;
        mf->backend.write=fwrite;
        mf->backend.close=fclose;
        mf->backend.gets=fgets;
        mf->backend.tell=ftell;
        mf->backend.seek=fseek;
    }else{
        mf->backend.closure=NULL;
        mf->backend.use=0;
    }
    return mf;
}
int mlinked(EWF_MEMFILE* mf){
    return mf->backend.use;
}
/*=============================*/
#pragma endregion
#pragma region EWF_LL
/*EWF_LL*/
EWF_LL*     EWF_LL_New          (void* data){
    EWF_LL* ret=(EWF_LL*)calloc(1,sizeof(EWF_LL));
    ret->data=data;
    return ret;
}
EWF_LL* EWF_LL_Orphan       (EWF_LL* head,EWF_LL* ll){
    if(ll){
        if(ll->prev==ll->next){
            ll->prev=ll->next=NULL;
        }else if(ll->prev && !ll->prev->next){
            //head node
            ll->next->prev=ll->prev;
            head=ll->next;
        }
        else if(!ll->next){
            //tail node
            head->prev=ll->prev;
            ll->prev->next=NULL;
        }else{
            //middle
            ll->prev->next=ll->next;
            ll->next->prev=ll->prev;
        }
        ll->prev=ll->next=NULL;
    }
    return head;
}
int         EWF_LL_GetSize      (EWF_LL* head){
    EWF_LL* cur=head;
    int l=0;
    if(cur==NULL)
        return 0;
    while(cur!=NULL){
        l++;
        cur=cur->next;
    }
    return l;
}
EWF_LL*     EWF_LL_Attach       (EWF_LL* head,EWF_LL* attach){
    if(head==NULL)
        head=attach; // attach is the first
    else if(head->prev==NULL){ // head is also tail
        head->prev=attach;
        head->next=attach;
    }
    else{ //replace tail
        EWF_LL* tail=head->prev;
        tail->next=attach;
        attach->prev=tail;
        head->prev=attach;
        attach->next=NULL;
    }
    return head;
}
void        EWF_LL_Map          (EWF_LL* head,EWF_MapFxn map,void* upvalue){
    EWF_LL* cur=head;
    while(cur){
        EWF_LL* temp=cur->next;
        map(NULL,cur,upvalue);
        cur=temp;
    }
}
int         EWF_LL_DefaultFree  (void* unused,void* vll,void* upvalue){
    EWF_LL* ll=(EWF_LL*)vll;
    free(ll->data);
    free(ll);
    return 0;
}
/*================================*/
#pragma endregion
#pragma region EWF_Array
/*EWF_Array*/
unsigned int    _EWF_Array_GetNewSize   (EWF_Array* arr){
    if(arr->resizeFxn==NULL)
        return (arr->cap==0?1:arr->cap*2);
    else
        return arr->resizeFxn(arr,arr->cap,arr->count);
}
EWF_Array*  EWF_Array_New   (unsigned int cap,EWF_Array_ResizeFxn resizeFxn){
    EWF_Array* ret=(EWF_Array*)calloc(sizeof(EWF_Array),1);
    ret->cap=cap;
    ret->resizeFxn=resizeFxn;
    ret->data=(void**)calloc(sizeof(void*),cap);
    return ret;
}
void**  EWF_Array_Push  (EWF_Array* arr,void* data){
    arr->count++;
    if(arr->count>arr->cap){
        arr->cap=_EWF_Array_GetNewSize(arr);
        arr->data=(void**)realloc(arr->data,sizeof(void*)*arr->cap);
        memset(arr->data+arr->count,'\0',sizeof(void*)*(arr->cap-arr->count));
    }
    arr->data[arr->count]=data;
    return (arr->data+arr->count);
}
void**  EWF_Array_Index (EWF_Array* arr,unsigned int idx){
    if(idx>arr->cap)
        return NULL;
    return (arr->data+idx);
}
void    EWF_Array_Free  (EWF_Array* arr){
    free(arr->data);
    free(arr);
}
/*================================*/
#pragma endregion
#pragma region EWF_ClassicDict
/*EWF_ClassicDict*/
EWF_ClassicDict* EWF_ClassicDict_New(int startingCap){
    EWF_ClassicDict* ret=(EWF_ClassicDict*)memset(malloc(sizeof(EWF_ClassicDict)),'\0',sizeof(EWF_ClassicDict));
    if(startingCap<=0)
        startingCap=2;
    ret->_capacity=startingCap;
    ret->keys=(char**)memset(malloc(sizeof(char*)*ret->_capacity),'\0',sizeof(char*)*ret->_capacity);
    ret->values=(void**)memset(malloc(sizeof(void*)*ret->_capacity),'\0',sizeof(void*)*ret->_capacity);
    ret->_size=0;
    return ret;
}
int EWF_ClassicDict_Expand(EWF_ClassicDict* dict){
    int oldCap=dict->_capacity;
    dict->_capacity*=2;
    dict->keys=(char**)realloc((void*)dict->keys,sizeof(char*)*dict->_capacity);
    memset((void*)(dict->keys+oldCap),'\0',sizeof(char*)*(dict->_capacity-oldCap));
    dict->values=(void**)realloc(dict->values,sizeof(void*)*dict->_capacity);
    return dict->_capacity;
}
int EWF_ClassicDict_GetIndexOf(EWF_ClassicDict* dict,char* key){
    int i=0;
    for(i=0;i<dict->_capacity;i++){
        if(dict->keys[i]==NULL)
            continue;
        if(strcmp(dict->keys[i],key)==0)
            return i;
    }
    return -1;
}
void* EWF_ClassicDict_Update(EWF_ClassicDict* dict,char* key,void* newVal){
    int i=EWF_ClassicDict_GetIndexOf(dict,key);
    if(i==-1)
        return NULL;
    dict->values[i]=newVal;
    return newVal;
}
void* EWF_ClassicDict_Insert(EWF_ClassicDict* dict,char* key,void* value){
    int firstFreeIndex=0;
    int i=0;
    int testI=EWF_ClassicDict_GetIndexOf(dict,key);
    if(testI!=-1)
        return EWF_ClassicDict_Update(dict,key,value);
    if(dict->_size>=dict->_capacity)
        EWF_ClassicDict_Expand(dict);
    for(i=0;i<dict->_capacity;i++){
        if(dict->keys[i]==NULL){
            firstFreeIndex=i;
            break;
        }
    }
    {
        size_t slen=strlen(key)+1;
        char* newKey=(char*)memset(malloc(sizeof(char)*slen),'\0',sizeof(char)*slen);
        memcpy((void*)newKey,(void*)key,slen);
        dict->keys[firstFreeIndex]=newKey;
        dict->values[firstFreeIndex]=value;
    }
    dict->_size++;
    return value;
}
void* EWF_ClassicDict_Get_k(EWF_ClassicDict* dict,char* key){
    int i=EWF_ClassicDict_GetIndexOf(dict,key);
    if(i==-1)
        return NULL;
    return dict->values[i];
}
void* EWF_ClassicDict_Get_i(EWF_ClassicDict* dict,int index){
    return dict->values[index];
}
char* EWF_ClassicDict_Delete(EWF_ClassicDict* dict,char* key){
    int i=EWF_ClassicDict_GetIndexOf(dict,key);
    char* oldKey=NULL;
    if(i==-1)
        return NULL;
    oldKey=dict->keys[i];
    free(oldKey);
    dict->keys[i]=NULL;
    dict->values[i]=NULL;
    return key;
}
void EWF_ClassicDict_Free(EWF_ClassicDict* dict,int freeValuesByLibC){
    int i=0;
    for(i=0;i<dict->_size;i++)
        if(dict->keys[i]!=NULL)
            free(dict->keys[i]);
    free(dict->keys);
    if(freeValuesByLibC)
        for(i=0;i<dict->_size;i++)
            if(dict->values[i]!=NULL)
                free(dict->values[i]);
    free(dict->values);
    dict->_size=dict->_capacity=0;
    dict->values=NULL;
    dict->keys=NULL;
}
/*================================*/
#pragma endregion
#pragma region EWF_Queue
/*Queue*/
EWF_Queue*  EWF_Queue_New   (unsigned int capacity){
    EWF_Queue* q=calloc(1,sizeof(*q));
    q->capacity=capacity;
    q->_mutex=EWFP_Mutex_New();
    q->_cond=EWFP_Condition_New();
    return q;
}
EWF_Queue*  EWF_Queue_Combine       (EWF_Queue* receiver,EWF_Queue* sacrifice){
    // want to attach sacrifice's LL to receiver's LL, and nuke sacrifice LL
    // so we set receiver->tail->next=sacrifice->top
    // and receiver->head->prev=sacrifice->tail
    EWF_LL* rTail=receiver->top->prev;
    EWF_LL* sTail=sacrifice->top->prev;
    rTail->next=sacrifice->top;
    receiver->top->prev=sTail;
    sacrifice->top->prev=rTail;
    sacrifice->top=NULL;
    sacrifice->_count=0;
    return receiver;
}
void*       EWF_Queue_Push_block    (EWF_Queue* q,void* data){
    while(EWF_Queue_Push_noblock(q,data)!=NULL);
    return NULL;
}
void*       EWF_Queue_Push_noblock  (EWF_Queue* q,void* data){
    EWF_LL* newE=NULL;
    if(q->capacity>0 && q->_count>=q->capacity)
        return data;
    newE=EWF_LL_New(data);
    EWFP_Mutex_Lock(q->_mutex);
    q->top=EWF_LL_Attach(q->top,newE);
    q->_count++;
    EWFP_Condition_Signal(q->_cond);
    EWFP_Mutex_Unlock(q->_mutex);
    return NULL;
}
void*       EWF_Queue_Pop_block (EWF_Queue* q){
    EWF_LL* temp=q->top;
    void* ret=NULL;
    EWFP_Mutex_Lock(q->_mutex);
    while(q->top==NULL)
        EWFP_Condition_Wait(q->_cond,q->_mutex,0);
    ret=q->top->data;
    q->top=q->top->next;
    if(q->top)
        q->top->prev=temp->prev;    // preserve tail
    free(temp);
    q->_count--;
    EWFP_Mutex_Unlock(q->_mutex);
    return ret;
}
void*       EWF_Queue_Pop_noblock   (EWF_Queue* q){
    EWF_LL* temp=q->top;
    void* ret=NULL;
    if(temp==NULL)
        return NULL;
    EWFP_Mutex_Lock(q->_mutex);
    ret=q->top->data;
    q->top=q->top->next;
    if(q->top)
        q->top->prev=temp->prev;    // preserve tail
    free(temp);
    q->_count--;
    EWFP_Mutex_Unlock(q->_mutex);
    return ret;
}
void*       EWF_Queue_Peek  (EWF_Queue* q){
    void* ret=NULL;
    if(q==NULL)
        return NULL;
    EWFP_Mutex_Lock(q->_mutex);
    if(q->top!=NULL){
        ret=q->top->data;
    }
    EWFP_Mutex_Unlock(q->_mutex);
    return ret;
}
void        EWF_Queue_Free  (EWF_Queue* q){
    //clear out the queue
    {
        EWF_LL* cur=q->top;
        while(cur!=NULL){
            EWF_LL* temp=cur;
            cur=cur->next;
            free(temp);
        }
    }
    //free the thread stuff
    EWFP_Mutex_Free(q->_mutex);
    EWFP_Condition_Free(q->_cond);
    free(q);
}
unsigned int    EWF_Queue_Count (EWF_Queue* q){
    return q->_count;
}
void        EWF_Queue_Map           (EWF_Queue* head,EWF_MapFxn map,void* upvalue){
    EWF_LL_Map(head->top,map,upvalue);
}
/*================================*/
#pragma endregion
#pragma region EWF_Logger
/*Logger*/
#define _ll_get(ptr,index) ((int*)ptr)[index]
void* llnew(void* d){void* r=memset(malloc(3*sizeof(int)),'\0',3*sizeof(int));_ll_get(r,2)=(int)d;return r;}
void* llatt(void* h,void* n){void* c=NULL;if(h==NULL)h=n;c=h;while(_ll_get(c,1)!=(int)NULL)c=(void*)_ll_get(c,1);
    if(c!=n){_ll_get(c,1)=(int)n;_ll_get(n,0)=(int)c;}return h;}
void* llnext(void* e){return (void*)_ll_get(e,1);}
void* lldata(void* e){return (void*)_ll_get(e,2);}
#undef _ll_get

EWF_MEMFILE*        EWF_Logger_AddDestination(EWF_Logger* log,EWF_MEMFILE* dest){
    if(dest!=NULL){
        EWF_LL* l=EWF_LL_New((void*)dest);
        log->destinations=EWF_LL_Attach(log->destinations,l);
    }
    return dest;
}
EWF_Logger* EWF_Logger_New(const char* name){
    EWF_Logger* ret=(EWF_Logger*)memset(malloc(sizeof(EWF_Logger)),'\0',sizeof(EWF_Logger));
    ret->name=(char*)malloc(sizeof(char)*(strlen(name)+1));
    strncpy(ret->name,name,strlen(name)+1);
    #ifndef _LOGGER_NO_STDOUT
    EWF_Logger_AddDestination(ret,mlink(mnew(),stdout));
    #endif
    ret->messageQueue=EWF_Queue_New(0);
    return ret;
}
void        EWF_Logger_Free(EWF_Logger* log){
    EWF_LL* cur=log->destinations;
    free(log->name);
    log->name=NULL;
    while(cur!=NULL){
        EWF_LL* temp=cur;
        cur=cur->next;
        free(temp);
    }
    EWF_Queue_Free(log->messageQueue);
    free(log);
}
void        EWF_Logger_Hold(EWF_Logger* log){
    log->savingMessages=1;
}
void        EWF_Logger_Release(EWF_Logger* log){
    log->savingMessages=0;
    EWF_Logger_Flush(log);
}
void        EWF_Logger_Flush(EWF_Logger* log){
    char* cur=(char*)EWF_Queue_Pop_noblock(log->messageQueue);
    while(cur!=NULL){
        EWF_LL* curd=log->destinations;
        while(curd!=NULL){
            mwrite(cur,sizeof(char),strlen(cur),(EWF_MEMFILE*)curd->data);
            curd=curd->next;
        }
        free(cur);
        cur=(char*)EWF_Queue_Pop_noblock(log->messageQueue);
    }
}
#define lmll EWF_LOGGER_MAX_LOG_LENGTH
void        EWF_Logger_Log_Ex(EWF_Logger* log,const char* level,const char* fxn,int line,const char* format,...){
    char* msg=NULL;
    va_list ap;
    EWF_LL* cur=log->destinations;
    if(level[0]==log->filter)
        return;
    msg=(char*)memset(malloc(lmll),'\0',lmll);
    va_start(ap,format);
    vsnprintf(msg,lmll,format,ap);
    va_end(ap);
    if(!log->savingMessages){
        while(cur!=NULL){
            EWF_MEMFILE* dest=(EWF_MEMFILE*)cur->data;
            mprintf(dest,"%s[%s](%s@%d): %s\n",log->name,level,fxn,line,msg);
            cur=cur->next;
        }
    }else{
        char* msg2=(char*)calloc(lmll,1);
        sprintf(msg2,"%s[%s](%s@%d): %s\n",log->name,level,fxn,line,msg);
        EWF_Queue_Push_block(log->messageQueue,msg2);
    }
    free(msg);
}
void        EWF_Logger_Log(EWF_Logger* log,const char* level,const char* format,...){
    char* msg=NULL;
    va_list ap;
    EWF_LL* cur=log->destinations;
    if(level[0]==log->filter)
        return;
    msg=(char*)memset(malloc(lmll),'\0',lmll);
    va_start(ap,format);
    vsnprintf(msg,lmll,format,ap);
    va_end(ap);
    if(!log->savingMessages){
        while(cur!=NULL){
            EWF_MEMFILE* dest=(EWF_MEMFILE*)cur->data;
            mprintf(dest,"%s[%s]: %s\n",log->name,level,msg);
            cur=cur->next;
        }
    }else{
        char* msg2=(char*)calloc(lmll,1);
        sprintf(msg2,"%s[%s]: %s\n",log->name,level,msg);
        EWF_Queue_Push_block(log->messageQueue,msg2);
    }
    free(msg);
}
#undef lmll
void        EWF_Logger_FilterOut(EWF_Logger* log,const char* filter){
    if(log==NULL)
        return;
    if(filter==NULL)
        log->filter=0;
    else{
        log->filter=filter[0];
    }
}
void        EWF_Logger_MapDestinations  (EWF_Logger* log,EWF_MapFxn map,void* upvalue){
    EWF_LL_Map(log->destinations,map,upvalue);
}
int             EWF_Logger_DefaultDestinationMap    (void* log,void* vll,void* upvalue){
    mclose((EWF_MEMFILE*)((EWF_LL*)vll)->data);
    free(vll);
    return 0;
}
void        EWF_Logger_MapMessages      (EWF_Logger* log,EWF_MapFxn map,void* upvalue){
    EWF_Queue_Map(log->messageQueue,map,upvalue);
}
/*=======================*/
#pragma endregion
#pragma region EWF_getopt
/*Getopt*/

const int   EWF_getopt_no_argument      =0;
const int   EWF_getopt_required_argument=1;
const int   EWF_getopt_optional_argument=2;

char*   optarg;
int     optind,opterr,optopt;
static void EWF_getopt_push_back(int argc,char** argv,int toPushBack){
    int i=0;
    char* temp=argv[toPushBack];
    for(i=toPushBack+1;i<argc;i++)
        argv[i-1]=argv[i];
    argv[argc]=temp;
}
static void EWF_getopt_trim(char** str){
    int i=0,l=strlen(*str);
    char* sp=NULL;
    while((**str==' '||**str=='\t')&&i<l){
        (*str)++;
        i++;
    }
    sp=*str;
    l=strlen(sp);
    for(i=l-1;i>=0;i--)
        if(sp[i]!=' '&&sp[i]!='\t'){
            sp[i+1]='\0';
            break;
        }
}
int EWF_getopt  (int in_argc,char* argv[],const char* optstring,
                    char** arg,int* ind,int* err,int* opt){
    static int currentIndexStorage=1;
    int currentIndex=1;
    static int argc=0;
    int curChar=0;
    int ret=0;
    size_t optstringlen=strlen(optstring);
    if(ind!=NULL){
        currentIndex=*ind;
        if(*ind==0){
            currentIndex=1;
            currentIndexStorage=currentIndex;
            argc=0;
        }
    }else
        currentIndex=currentIndexStorage;
    if(argc==0)
        argc=in_argc;
    if(arg!=NULL)
        *arg=NULL;
    for(;currentIndex<argc;currentIndex++){
        EWF_getopt_trim((char**)&(argv[currentIndex]));
        if(argv[currentIndex][0]!='-'){
            if(optstring[0]=='+')
                break; //kick to scan-end
            if(optstring[0]=='-'){
                *arg=argv[currentIndex];
                currentIndex++;
                currentIndexStorage=currentIndex;
                ret=1;
                goto out;
            }
            EWF_getopt_push_back(argc,(char**)argv,currentIndex);
            currentIndex--;
            argc--;
            continue;
        }
        if(strcmp("--",argv[currentIndex])==0) //kill scanning
            break;
        currentIndexStorage=currentIndex+1;
        for(curChar=0;curChar<optstringlen;curChar++)
            if(optstring[curChar]==argv[currentIndex][1])
                break;
        if(curChar==optstringlen){
            if(opt!=NULL)
                *opt=argv[currentIndex][1];
            if(optstring[0]==':'){
                ret=':';
                goto out;
            }
            ret='?';
            goto out;
        }
        if(optstring[curChar+1]==':'){
            //required arg
            if(curChar+2<optstringlen && optstring[curChar+2]==':'){
                //optional arg
                if(strlen(argv[currentIndex])>2)
                    *arg=argv[currentIndex]+2;
                else
                    *arg=NULL;
            }else{
                if(strlen(argv[currentIndex])<=2){
                    if(currentIndex+1>=argc){ //missing arg
                        if(opt!=NULL)
                            *opt=argv[currentIndex][1];
                        if(optstring[0]==':'){
                            ret=':';
                            goto out;
                        }
                        ret='?';
                        goto out;
                    }
                    *arg=argv[currentIndex+1];
                    currentIndex+=2;
                    currentIndexStorage+=2;
                }else{
                    *arg=argv[currentIndex]+2;
                }
            }
        }
        ret=optstring[curChar];
        goto out;
    }
    //scanning's over, so reset
    if(ind!=NULL)
        *ind=0;
    return -1;
out:
    *ind=currentIndex;
    return ret;
}
int EWF_getopt_long     (int argc,char* argv[],const char* optstring,
                            const EWF_option* longopts,int* longindex,
                            char** arg,int* ind,int* err,int* opt){
    return -1;
}

int EWF_getopt_long_only    (int in_argc,char* argv[],const char* optstring,
                                const EWF_option* longopts,int* longindex,
                                char** arg,int* ind,int* err,int* opt){
    static int currentIndexStorage=1;
    int currentIndex=1;
    static int argc=0;
    int curChar=0;
    size_t optstringlen=strlen(optstring);
    int curOpt=0;
    int ret=0;
    if(longindex)
        *longindex=-1;
    if(ind!=NULL){
        currentIndex=*ind;
        if(*ind==0){
            currentIndex=1;
            currentIndexStorage=currentIndex;
            argc=0;
        }
    }else
        currentIndex=currentIndexStorage;
    if(argc==0)
        argc=in_argc;
    if(arg!=NULL)
        *arg=NULL;
    for(;currentIndex<argc;currentIndex++){
        char* eqPos=NULL;
        EWF_getopt_trim((char**)&(argv[currentIndex]));
        if(argv[currentIndex][0]!='-'){
            if(optstring[0]=='+')
                break; //kick to scan-end
            if(optstring[0]=='-'){
                *arg=argv[currentIndex];
                currentIndex++;
                currentIndexStorage=currentIndex;
                ret=1;
                goto out;
            }
            EWF_getopt_push_back(argc,(char**)argv,currentIndex);
            currentIndex--;
            argc--;
            continue;
        }
        if(strcmp("--",argv[currentIndex])==0) //kill scanning
            break;
        currentIndexStorage=currentIndex+1;
        { //first, look for matching long opt
            size_t argvStart=(strrchr(argv[currentIndex],'-')+1)-argv[currentIndex];
            eqPos=strchr(argv[currentIndex],'=');
            while(longopts[curOpt].name!=NULL){
                if(eqPos!=NULL){
                    if(strncmp(argv[currentIndex]+argvStart,longopts[curOpt].name,eqPos-argv[currentIndex]-argvStart)==0)
                        break;
                }else{
                    if(strcmp(argv[currentIndex]+argvStart,longopts[curOpt].name)==0)
                        break;
                }
                curOpt++;
            }
            if(longopts[curOpt].name!=NULL){ //got a long opt match
                if(longindex)
                    *longindex=curOpt;
                if(longopts[curOpt].flag)
                    *(longopts[curOpt].flag)=longopts[curOpt].val;
                if(longopts[curOpt].has_arg==EWF_getopt_required_argument){
                    if(eqPos==NULL && (currentIndex+1)>=argc){ //arg required, not present in current argv
                        if(optstring && optstring[0]==':'){
                            ret=':';
                            goto out;
                        }else{
                            ret='?';
                            goto out;
                        }
                    }else{
                        if(eqPos)
                            *arg=eqPos+1;
                        else{
                            *arg=argv[currentIndex+1];
                            currentIndex+=2;
                            currentIndexStorage+=2;
                        }
                    }
                }else if(longopts[curOpt].has_arg==EWF_getopt_optional_argument){
                    if(eqPos){
                        *arg=eqPos+1;
                    }else if((currentIndex+1)<argc && argv[currentIndex+1][0]!='-'){
                        *arg=argv[currentIndex+1];
                        currentIndex+=2;
                        currentIndexStorage+=2;
                    }
                }
                if(longopts[curOpt].flag){
                    ret=0;
                    goto out;
                }
                ret=longopts[curOpt].val; //return since we found a long option
                goto out;
            }
        }
        //no long match found, look for short match
        for(curChar=0;curChar<optstringlen;curChar++)
            if(optstring[curChar]==argv[currentIndex][1])
                break;
        if(curChar==optstringlen){
            if(opt!=NULL)
                *opt=argv[currentIndex][1];
            if(optstring[0]==':'){
                ret=':';
                goto out;
            }
            ret='?';
            goto out;
        }
        if(optstring[curChar+1]==':'){
            //required arg
            if(curChar+2<optstringlen && optstring[curChar+2]==':'){
                //optional arg
                if(strlen(argv[currentIndex])>2)
                    *arg=argv[currentIndex]+2;
                else
                    *arg=NULL;
            }else{
                if(strlen(argv[currentIndex])<=2){
                    *arg=argv[currentIndex+1];
                    currentIndex+=2;
                    currentIndexStorage+=2;
                }else{
                    *arg=argv[currentIndex]+2;
                }
            }
        }
        ret=optstring[curChar];
        goto out;
    }
    //scanning's over, so reset
    if(ind!=NULL)
        *ind=0;
    return -1;
out:
    *ind=currentIndex;
    return ret;
}
/*================================*/
#pragma endregion
#pragma region EWF_INI
/*INI File Reading*/
#define INI_LINE_LENGTH 256
char* _INI_ProcessLine(char* line,int lower){
    size_t lineLen=strlen(line);
    int i=0;
    //first, kill trailing whitespace
    for(i=0;i<lineLen;i++)
        line[i]=tolower(line[i]);
    for(i=lineLen-1;i>=0;i--){
        if(line[i]!= ' ' && line[i]!='\t' && line[i]!='\n')
            break;
    }
    if(i==0){
        //whole line is whitespace
        line[0]='\0';
        return line; //return empty line
    }
    //terminate at whitespace
    line[i+1]='\0';
    //now, leading whitespace
    for(i=0;i<lineLen;i++){
        if(line[i]!= ' ' && line[i]!='\t' && line[i]!='\n')
            break;
    }
    if(i!=0){
        //there's some leading ws
        //shift chars over
        size_t shiftNum=i;
        for(i=0;i<lineLen-shiftNum+1;i++) //make sure null-term
            line[i]=line[i+shiftNum];
    }
    return line;
}
EWF_INI_File* EWF_INI_Parse(EWF_MEMFILE* stream,int hierarchy,int lowercase){
    EWF_INI_File* ret=NULL;
    char* line=NULL;
    size_t lineLen=0;
    char* currentSection=NULL;
    const int currentSectionStartingLen=512;
    size_t currentSectionLen=currentSectionStartingLen;
    size_t currentSectionIndex=0;
    if(stream==NULL)
        return NULL;
    line=memset(malloc(sizeof(char)*INI_LINE_LENGTH),'\0',sizeof(char)*INI_LINE_LENGTH);
    currentSection=memset(malloc(sizeof(char)*currentSectionStartingLen),'\0',sizeof(char)*currentSectionStartingLen);
    ret=memset(malloc(sizeof(EWF_INI_File)),'\0',sizeof(EWF_INI_File));
    while(mgets(line,INI_LINE_LENGTH,stream)!=NULL){
        line=_INI_ProcessLine(line,lowercase);
        //time to parse
        if(line[0]==';' || line[0]=='#'){ //comment or directive
            const static char* delimiter="--directive";
            const static int delimiterLen=11;
            int i=0;
            for(i=0;i<delimiterLen;i++){
                if(line[1+i]!=delimiter[i])
                    break;
            }
            if(i==delimiterLen){ //found directive
            }
            //TODO: finish directives
            continue;
        }
        lineLen=strlen(line);
        if(line[0]=='['){ //section decl
            //first, see if it's a closing section
            //TODO: watch hierarchy behavior
            if(!hierarchy){
                strncpy(currentSection,line+1,lineLen-1-1);
                continue;
            }
            if(line[1]=='/'){
                int dotIndex=0;
                dotIndex=(int)strrchr(currentSection,'.');
                if(dotIndex==0){
                    currentSection[0]='\0';
                    currentSectionIndex=0;
                }else{
                    char* dotPtr=(char*)dotIndex;
                    *dotPtr='\0';
                    currentSectionIndex=dotPtr-currentSection;
                }
                continue;
            }
            if(currentSectionIndex+lineLen>currentSectionLen){
                void* ret=NULL;
                ret=realloc(currentSection,sizeof(char)*(currentSectionIndex+lineLen+currentSectionStartingLen));
                if(ret)
                    currentSection=ret;
                currentSectionLen=currentSectionIndex+lineLen+currentSectionStartingLen;
            }
            //kill ending bracket
            line[--lineLen]='\0';
            if(currentSectionIndex!=0)
                currentSection[currentSectionIndex++]='.';
            strncpy(currentSection+currentSectionIndex,line+1,lineLen-1-1); //1 for null term and 1 for bracket
            currentSectionIndex+=lineLen-1-1;
            currentSection[currentSectionIndex]='\0';
        }else{ //property/value pair
            char* equalPtr=strchr(line,'=');
            char* name=NULL;
            char* value=NULL;
            size_t nameLen=0;
            size_t rawNameLen=0;
            size_t valueLen=0;
            if(equalPtr!=NULL){
                *equalPtr='\0';
                rawNameLen=strlen(line);
                nameLen=rawNameLen+currentSectionIndex+1;
                valueLen=strlen(equalPtr+1);
                name=memset(malloc(sizeof(char)*nameLen+1),'\0',sizeof(char)*nameLen+1);
                value=memset(malloc(sizeof(char)*valueLen+1),'\0',sizeof(char)*valueLen+1);
                if(currentSectionIndex!=0){
                    strncpy(name,currentSection,currentSectionIndex);
                    name[currentSectionIndex]='.';
                    strncpy(name+currentSectionIndex+1,line,rawNameLen);
                }else
                    strncpy(name,line,rawNameLen);
                strncpy(value,equalPtr+1,valueLen);
                if(ret->values==NULL)
                    ret->values=EWF_ClassicDict_New(8);
                EWF_ClassicDict_Insert(ret->values,name,value);
                free(name); //dict creates its own copy of name
            }
        }
    }
    ret->stream=stream;
    ret->_lowercase=lowercase;
    return ret;
}
void EWF_INI_Free(EWF_INI_File* ini,int closeFile){
    if(ini==NULL)
        return;
    if(closeFile)
        mclose(ini->stream);
    EWF_ClassicDict_Free(ini->values,1);
}
char* EWF_INI_Get(EWF_INI_File* ini,char* name){
    char* ret=NULL;
    if(ini==NULL)
        return NULL;
    ret=(char*)EWF_ClassicDict_Get_k(ini->values,name);
    return ret;
}
/*=======================*/
#pragma endregion
#pragma region EWF_BTree
static int  _EWF_BTree_Default_Compare  (void* k1,void* k2){
    return ((int)k1)-((int)k2);
}
EWF_BTree_Node* EWF_BTree_Node_New  (void* key,void* val){
    EWF_BTree_Node* ret=calloc(1,sizeof(*ret));
    ret->key=key;
    ret->value=val;
    return ret;
}
void            EWF_BTree_Node_Free (EWF_BTree_Node* n){
    free(n);
}
EWF_BTree*      EWF_BTree_New   (){
    EWF_BTree* ret=calloc(1,sizeof(*ret));
    ret->compareKeys=_EWF_BTree_Default_Compare;
    return ret;
}
//algorithm from http://en.wikipedia.org/wiki/Binary_search_tree#Deletion
EWF_BTree_Node* EWF_BTree_Remove    (EWF_BTree* bt,EWF_BTree_Node* n){
    //ensure tree balance
    EWF_BTree_Node* ret=n;
    if(bt->dirty)
        EWF_BTree_Balance(bt);
    if(n){
        if(!n->left && !n->right){
            //leaf
            if(n->parent->left==n)
                n->parent->left=NULL;
            if(n->parent->right==n)
                n->parent->right=NULL;
        }else if(n->left && n->right){
            //two children
            EWF_BTree_Node* repl=n;
            EWF_BTree_Node temp={0};
            if(rand()%2){
                //use in-order successor
                while(repl->right)
                    repl=repl->right;
            }else{
                //use in-order predecessor
                while(repl->left)
                    repl=repl->left;
            }
            temp.key=n->key;
            temp.value=n->value;
            n->key=repl->key;
            n->value=repl->value;
            ret=EWF_BTree_Remove(bt,n);
        }else{
            //one child
            EWF_BTree_Node** ppReplace=NULL;
            if(n->parent->left==n)
                ppReplace=&n->parent->left;
            if(n->parent->right==n)
                ppReplace=&n->parent->right;
            if(n->left)
                *ppReplace=n->left;
            if(n->right)
                *ppReplace=n->right;
        }
        n->parent=NULL;
    }
    bt->dirty=1;
    return ret;
}
EWF_BTree_Node* EWF_BTree_Insert    (EWF_BTree* bt,EWF_BTree_Node* n){
    EWF_BTree_Node* cur=bt->root;
    if(!cur)
        bt->root=n;
    while(cur){
        //if n->key < cur->key, cur=cur->left
        //if n->key > cur->key, cur=cur->right
        //if n->key == cur->key, replace?
        int compResult=(bt->compareKeys?
            bt->compareKeys(n->key,cur->key):
        _EWF_BTree_Default_Compare(n->key,cur->key));
        if(compResult<0){
            if(!cur->left){
                cur->left=n;
                n->parent=cur;
                break;
            }else
                cur=cur->left;
        }else if(compResult>0){
            if(!cur->right){
                cur->right=n;
                n->parent=cur;
                break;
            }else
                cur=cur->right;
        }else{
            cur->value=n->value;
            break;
        }
    }
    bt->dirty=1;
    return n;
}
EWF_BTree_Node* EWF_BTree_Lookup        (EWF_BTree* bt,void* key){
    EWF_BTree_Node* cur=bt->root;
    while(cur){
        int compResult=(bt->compareKeys?
            bt->compareKeys(key,cur->key):
        _EWF_BTree_Default_Compare(key,cur->key));
        if(compResult<0)
            cur=cur->left;
        else if(compResult>0)
            cur=cur->right;
        else
            break;
    }
    return cur;
}
void    EWF_BTree_Print (EWF_BTree_Node* n,unsigned int level){
    printf(" %d ",(int)level);
    printf("0x%08x=%c\n",n->key,n->value);
    if(n->left){
        printf(" l ");
        EWF_BTree_Print(n->left,level+1);
    }
    if(n->right){
        printf(" r ");
        EWF_BTree_Print(n->right,level+1);
    }
    fflush(stdout);
}
void            EWF_BTree_Balance_Compress  (EWF_BTree_Node* psRoot,unsigned int count){
    EWF_BTree_Node* scanner;
    EWF_BTree_Node* child;
    unsigned int i=0;
    scanner=psRoot;
    for(i=0;i<count;i++){
        child=scanner->right;
        scanner->right=child->right;
        scanner=scanner->right;
        child->right=scanner->left;
        scanner->left=child;
    }
}
void            EWF_BTree_Balance       (EWF_BTree* bt){
    //based on "Tree Rebalancing in Optimal Time and Space": Communications of the ACM 29 (1986), pp. 902-908
    unsigned int size=0;
    EWF_BTree_Node* vineTail=NULL;
    EWF_BTree_Node* remainder=NULL;
    if(bt->root==NULL)
        return;
    {
        //create pseudo-root
        EWF_BTree_Node* psRoot=EWF_BTree_Node_New(NULL,(void*)'r');
        psRoot->right=bt->root;
        bt->root->parent=psRoot;
        bt->root=psRoot;
    }
    { //Tree to vine
        vineTail=bt->root;
        remainder=vineTail->right;
        while(remainder){
            if(remainder->left==NULL){
                vineTail=remainder;
                remainder=remainder->right;
                size++;
            }else{
                EWF_BTree_Node* temp=remainder->left;
                remainder->left=temp->right;
                temp->right=remainder;
                remainder=temp;
                vineTail->right=temp;
            }
        }
    }
    { //Vine to tree
        unsigned int leafCount;
        {
            int lgSize=0;
            //note that this is a base 2 log!  paper does not say that
            float actualLgSize=(float)(log((float)size+1)/log(2.0f));
            lgSize=(int)floor(actualLgSize);
            leafCount=size+1-((int)pow((float)2,(float)lgSize));
        }
        EWF_BTree_Balance_Compress(bt->root,leafCount);
        size-=leafCount;
        while(size>1){
            EWF_BTree_Balance_Compress(bt->root,size/2);
            size/=2;
        }
    }
    { //kill pseudoroot
        EWF_BTree_Node* root=bt->root->right;
        EWF_BTree_Node_Free(bt->root);
        bt->root=root;
        root->parent=NULL;
    }
    bt->dirty=0;
}
void            _EWF_BTree_FreeSubtree  (EWF_BTree_Node* n){
    if(!n)
        return;
    _EWF_BTree_FreeSubtree(n->left);
    _EWF_BTree_FreeSubtree(n->right);
    EWF_BTree_Node_Free(n);
}
void            EWF_BTree_Free  (EWF_BTree* bt){
    //TODO: add a non-recursive mode
    _EWF_BTree_FreeSubtree(bt->root);
}
void            _EWF_BTree_MapSubtree   (EWF_BTree* bt,EWF_BTree_Node* n,EWF_MapFxn map,void* upvalue){
    if(n->left) _EWF_BTree_MapSubtree(bt,n->left,map,upvalue);
    if(n->right) _EWF_BTree_MapSubtree(bt,n->right,map,upvalue);
    map(bt,n,upvalue);
}
void            EWF_BTree_Map   (EWF_BTree* bt,EWF_MapFxn map,void* upvalue){
    //TODO: add a non-recursive mode
    _EWF_BTree_MapSubtree(bt,bt->root,map,upvalue);
}

EWF_BTree*      EWF_Dict_New    (){
    EWF_BTree* b=calloc(1,sizeof(*b));
    b->compareKeys=(EWF_BTree_CompareFxn)strcmp;
    return b;
}
#pragma endregion
#pragma region EWF_HashTable
static const unsigned int EWF_HASHTABLE_SEED=0x59519;
int             EWF_HashTable_Compare   (void* k1,void* k2){
    //assume k1 and k2 are unsigned int's
    if(k1 && k2)
        return ((unsigned int)k1)-((unsigned int)k2);
    return 0;
}
EWF_HashTable*  EWF_HashTable_New       (){
    EWF_HashTable* ret=calloc(1,sizeof(*ret));
    ret->compareKeys=EWF_HashTable_Compare;
    return ret;
}
void*           EWF_HashTable_Insert    (EWF_HashTable* ht,void* value,void* key,size_t keylen){
    unsigned int hash=0;
    if(ht){
        if(key){
            EWF_Hash_Murmur_x86_32(key,(int)keylen,EWF_HASHTABLE_SEED,(void*)&hash);
            EWF_BTree_Insert(ht,EWF_BTree_Node_New((void*)hash,value));
            return value;
        }
    }
    return NULL;
}
void*           EWF_HashTable_Lookup_k  (EWF_HashTable* ht,void* key,size_t keylen){
    unsigned int hash=NULL;
    if(ht){
        if(key){
            EWF_BTree_Node* n=NULL;
            EWF_Hash_Murmur_x86_32(key,(int)keylen,EWF_HASHTABLE_SEED,(void*)&hash);
            n=EWF_BTree_Lookup(ht,(void*)hash);
            if(n) return n->value;
        }
    }
    return NULL;
}
void*           EWF_HashTable_Lookup_h  (EWF_HashTable* ht,unsigned int hash){
    if(ht){
        EWF_BTree_Node* n=NULL;
        n=EWF_BTree_Lookup(ht,(void*)&hash);
        if(n) return n->value;
    }
    return NULL;
}
void            EWF_HashTable_Free      (EWF_HashTable* ht){
    EWF_BTree_Free(ht);
}
void            EWF_HashTable_Map       (EWF_HashTable* ht,EWF_MapFxn map,void* upvalue){
    EWF_BTree_Map(ht,map,upvalue);
}
#pragma endregion
#pragma region EWF_Block_File
void* EWF_Block_File_Metadata_Parse(unsigned short t,   //type
    unsigned char o,                                    //options
    unsigned int s,                                 //size
    EWF_MEMFILE* r,                                 //stream
    unsigned int* sz,                                   //szOut
    void* cl){
        EWF_Block_File_Metadata* m=calloc(1,sizeof(*m));
        *sz=sizeof(*m);
        mread(m,sizeof(unsigned int),4,r);
        return m;
}
void EWF_Block_File_Metadata_Write(unsigned short t,        //type
    unsigned char o,                                    //options
    void* d,                                            //data
    unsigned int s,                                 //size
    EWF_MEMFILE* r,                                 //stream
    unsigned int* sz,                                   //szOut
    void* cl){
        EWF_Block_File_Metadata* m=d;
        mwrite(m,sizeof(unsigned int),4,r);
        *sz=sizeof(unsigned int)*4;
}
void* EWF_Block_File_Comment_Parse(unsigned short t,    //type
    unsigned char o,                                    //options
    unsigned int s,                                 //size
    EWF_MEMFILE* r,                                 //stream
    unsigned int* sz,                                   //szOut
    void* cl){
        EWF_Block_File_Comment* c=calloc(1,sizeof(*c));
        unsigned int strSize=0;
        *sz=sizeof(*c);
        mread(&strSize,sizeof(strSize),1,r);
        if(strSize>0){
            c->name=calloc(strSize+1,1);
            mread(c->name,1,strSize,r);
        }
        mread(&strSize,sizeof(strSize),1,r);
        if(strSize>0){
            c->comment=calloc(strSize+1,1);
            mread(c->comment,1,strSize,r);
        }
        return c;
}
void EWF_Block_File_Comment_Write(unsigned short t,     //type
    unsigned char o,                                    //options
    void* d,                                            //data
    unsigned int s,                                 //size
    EWF_MEMFILE* r,                                 //stream
    unsigned int* sz,                                   //szOut
    void* cl){
        EWF_Block_File_Comment* c=d;
        size_t strSize;
        *sz=0;
        strSize=strlen(c->name);
        *sz+=sizeof(strSize);
        *sz+=strSize;
        mwrite(&strSize,sizeof(strSize),1,r);
        if(strSize>0)
            mwrite(c->name,1,strSize,r);
        strSize=strlen(c->comment);
        *sz+=sizeof(strSize);
        *sz+=strSize;
        mwrite(&strSize,sizeof(strSize),1,r);
        if(strSize>0)
            mwrite(c->comment,1,strSize,r);
}
EWF_Block_Block*    EWF_Block_Block_New (){
    return calloc(1,sizeof(EWF_Block_Block));
}
void            EWF_Block_Block_Free    (EWF_Block_Block* b){
    if(b->type<=0xFF){
        switch(b->type){
        case 0x01:
            {
                if(b->file->metadata==b->data)
                    b->file->metadata=NULL;
            }break;
        case 0x02:
            {
                EWF_Block_File_Comment* c=b->data;
                if(c->name) free(c->name);
                if(c->comment) free(c->comment);
            }break;
        }
        free(b->data);
    }
    free(b);
}
EWF_Block_File* EWF_Block_File_Open (EWF_MEMFILE* mf){
    EWF_Block_File* bf=calloc(1,sizeof(*bf));
    bf->stream=mf;
    { //initialize parsers with built-ins
        EWF_Block_Block_Defn* md=calloc(1,sizeof(*md));
        EWF_Block_Block_Defn* cd=calloc(1,sizeof(*cd));
        md->p=EWF_Block_File_Metadata_Parse;
        md->w=EWF_Block_File_Metadata_Write;
        cd->p=EWF_Block_File_Comment_Parse;
        cd->w=EWF_Block_File_Comment_Write;
        bf->blockDefns=EWF_BTree_New();
        EWF_BTree_Insert(bf->blockDefns,EWF_BTree_Node_New((void*)1,md));
        EWF_BTree_Insert(bf->blockDefns,EWF_BTree_Node_New((void*)2,cd));
        EWF_BTree_Balance(bf->blockDefns);
    }
    return bf;
}
int EWF_Block_File_Parse    (EWF_Block_File* bf){
    if(!bf)
        return -1;
    {
        char magic[5];
        mread(magic,sizeof(char),5,bf->stream);
        if(strncmp(magic,"EWFBF",5)!=0)
            return -2;
    }
    {
        unsigned int i=0;
        mread(&bf->blockCount,sizeof(bf->blockCount),1,bf->stream);
        for(;i<bf->blockCount;i++){
            EWF_Block_Block* b=calloc(1,sizeof(*b));
            EWF_LL* newLL=EWF_LL_New(b);
            mread(&b->type,sizeof(b->type),1,bf->stream);
            mread(&b->options,sizeof(b->options),1,bf->stream);
            mread(&b->size,sizeof(b->size),1,bf->stream);
            {
                EWF_Block_Block_Defn* d=NULL;
                unsigned int szOut=0;
                d=EWF_BTree_Lookup(bf->blockDefns,(void*)b->type)->value;
                if(d && d->p){
                    b->data=d->p(b->type,b->options,b->size,bf->stream,&szOut,d->pData);
                    b->size=szOut;
                }else{
                    b->data=calloc(1,b->size);
                    mread(b->data,b->size,1,bf->stream);
                }
            }
            bf->blocks=EWF_LL_Attach(bf->blocks,newLL);
        }
    }
    return 0;
}
int         EWF_Block_File_Write        (EWF_Block_File* bf){
    if(!bf)
        return -1;
    if(!bf->stream)
        return -2;
    {
        char* magic="EWFBF";
        mwrite(magic,sizeof(char),5,bf->stream);
    }
    mwrite(&bf->blockCount,sizeof(bf->blockCount),1,bf->stream);
    {
        unsigned int i=0;
        unsigned int wroteMetadata=0;
        EWF_LL* cur=bf->blocks;
        for(i=0;i<bf->blockCount;i++){
            EWF_Block_Block* b=cur->data;
            if(b){
                mwrite(&b->type,sizeof(b->type),1,bf->stream);
                mwrite(&b->options,sizeof(b->options),1,bf->stream);
                mwrite(&b->size,sizeof(b->size),1,bf->stream);
                {
                    EWF_Block_Block_Defn* d=NULL;
                    unsigned int szOut=0;
                    d=EWF_BTree_Lookup(bf->blockDefns,(void*)b->type)->value;
                    if(d && d->w){
                        d->w(b->type,b->options,b->data,b->size,bf->stream,&szOut,d->wData);
                    }else{
                        mwrite(b->data,b->size,1,bf->stream);
                    }
                }
            }
            cur=cur->next;
        }
    }
    return 0;
}
void        EWF_Block_File_Close        (EWF_Block_File* bf){
    unsigned int i=0;
    {
        EWF_LL* cur=bf->blocks;
        while(cur){
            EWF_LL* tempCur=cur;
            EWF_Block_Block_Free(cur->data);
            cur=cur->next;
            free(tempCur);
        }
    }
    bf->blockCount=0;
    bf->blocks=NULL;
    if(bf->metadata) free(bf->metadata);
}
void        EWF_Block_File_Free     (EWF_Block_File* bf){
    EWF_Block_File_Close(bf);
    EWF_BTree_Free(bf->blockDefns);
    mclose(bf->stream);
    free(bf);
}
#pragma endregion
