#include "ewfutilspp.h"

#include <time.h>

#include <pthread.h>

namespace EWF{
    namespace Utils{
        namespace Threads{

            struct Mutex::Private{
                pthread_mutex_t mutex;
            };
            Mutex::Mutex(){
                priv_ = new Mutex::Private();
                pthread_mutex_init(&priv_->mutex, NULL);
            }
            Mutex::~Mutex(){
                pthread_mutex_destroy(&priv_->mutex);
                delete priv_;
            }
            int Mutex::lock(){
                return pthread_mutex_lock(&priv_->mutex);
            }
            int Mutex::unlock(){
                return pthread_mutex_unlock(&priv_->mutex);
            }

            struct Condition::Private{
                pthread_cond_t  cond;
            };
            Condition::Condition(Mutex& mutex):mutex_(mutex){
                priv_ = new Condition::Private();
                pthread_cond_init(&priv_->cond, NULL);
            }
            Condition::~Condition(){
                pthread_cond_destroy(&priv_->cond);
                delete priv_;
            }
            int Condition::signal   (){
                return pthread_cond_signal(&priv_->cond);
            }
            int Condition::broadcast    (){
                return pthread_cond_broadcast(&priv_->cond);
            }
            int Condition::wait         (){
                return pthread_cond_wait(
                        &priv_->cond,
                        &mutex_.priv_->mutex);
            }

            struct Thread::Private{
                pthread_t   thread;
            };
            Thread::Thread(){
                state_ = Stopped;
                priv_ = new Thread::Private();
                pthread_create(&priv_->thread, NULL, Thread::workWrapper_, this);
            }
            Thread::~Thread(){
                stop(true);
                delete priv_;
            }
            void*   Thread::workWrapper_    (void* vThread){
                Thread* th = (Thread*)vThread;
                while(th->state() != Thread::Running);
                return (void*)th->work_();
            }
            int     Thread::start   (){
                state_ = Thread::Running;
                return 0;
            }
            int     Thread::pause   (){
                state_ = Thread::Paused;
                return 0;
            }
            int     Thread::stop    (bool force){
                if(force)
                    pthread_cancel(priv_->thread);
                else
                    state_ = Thread::Stopped;
                return 0;
            }
            int     Thread::join    () const{
                void* ret = NULL;
                pthread_join(priv_->thread, &ret);
                return (long long)ret;
            }
            Thread::State   Thread::state   () const{
                return state_;
            }

        }
        namespace Sys{

            unsigned long long GetTicks(){
                unsigned long long ret = 0;
                struct timespec spec = {0};
                clock_gettime(CLOCK_REALTIME, &spec);
                ret = (spec.tv_sec * 1000) + (spec.tv_nsec / 1e6);
                return ret;
            }

        }
    }
}
