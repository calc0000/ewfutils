#ifndef EWFUTILS_PP_H
#define EWFUTILS_PP_H

namespace EWF{
    namespace Utils{
        namespace Threads{

            class Mutex;

            class Condition{
                private:
                    struct      Private;
                    Private*    priv_;
                    Mutex&      mutex_;
                public:
                    Condition   (Mutex& mutex);
                    ~Condition  ();
                    int         signal      ();
                    int         broadcast   ();
                    int         wait        ();
            };

            class Mutex{
                private:
                    friend class Condition;
                    struct      Private;
                    Private*    priv_;
                public:
                    Mutex   ();
                    ~Mutex  ();
                    int     lock    ();
                    int     unlock  ();
            };

            class Thread{
                public:
                    enum State{
                        Unknown,
                        Stopped,
                        Paused,
                        Running,
                    };
                private:
                    struct Private;
                    Private* priv_;
                    State   state_;
                protected:
                    virtual int            work_    () = 0;
                    static void*             workWrapper_ (void* vThread);
                public:
                    Thread  ();
                    ~Thread ();
                    int             start   ();
                    //pause may not be honored
                    int             pause   ();
                    int             stop    (bool force = false);
                    int             join    () const;
                    State           state   () const;
            };

        }
        namespace Sys{
            
            //Returns milliseconds (useful only for delta measurements
            unsigned long long GetTicks();

        }
    }
}

#endif
