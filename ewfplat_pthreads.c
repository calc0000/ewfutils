#include "ewfplat.h"

#include <stdlib.h>
#include <pthreads.h>
#include <semaphore.h>
#include <time.h>
#include <unistd.h>

struct EWFP_Thread_Mutex{
	pthread_mutex_t*	mutex;
};
EWFP_Thread_Mutex* EWFP_Thread_Mutex_New(){
	EWFP_Thread_Mutex* m=calloc(1,sizeof(*m));
	m->mutex=calloc(1,sizeof(pthread_mutex_t));
	pthread_mutex_init(m->mutex,NULL);
	return m;
}
int EWFP_Thread_Mutex_Lock(EWFP_Thread_Mutex* mutex){
	pthread_mutex_lock(mutex->mutex);
	return 0;
}
int EWFP_Thread_Mutex_Unlock(EWFP_Thread_Mutex* mutex){
	pthread_mutex_unlock(mutex->mutex);
	return 0;
}
void EWFP_Thread_Mutex_Free(EWFP_Thread_Mutex* mutex){
	pthread_mutex_destroy(mutex->mutex);
	free(mutex->mutex);
	free(mutex);
}

struct EWFP_Thread_Condition{
	pthread_cond_t*	cond;
};
EWFP_Thread_Condition* EWFP_Thread_Condition_New(){
	EWFP_Thread_Condition* c=calloc(1,sizeof(*c));
	c->cond=calloc(1,sizeof(pthread_cond_t));
	pthread_cond_init(c->cond,NULL);
	return c;
}
int EWFP_Thread_Condition_Wait(EWFP_Thread_Condition* cond,EWFP_Thread_Mutex* mutex,unsigned int timeout){
	if(timeout==0)
		return pthread_cond_wait(cond->cond,mutex->mutex);
	else{
		timespec t;
		clock_gettime(CLOCK_REALTIME,&t);
		t.nanoseconds+=(timeout*1000000); //timeout ms from now
		return pthread_cond_timedwait(cond->cond,mutex->mutex,&t);
	}
}
int EWFP_Thread_Condition_Signal(EWFP_Thread_Condition* cond){
	return pthread_cond_signal(cond->cond);
}
int EWFP_Thread_Condition_Broadcast(EWFP_Thread_Condition* cond){
	return pthread_cond_broadcast(cond->cond);
}
void EWFP_Thread_Condition_Free(EWFP_Thread_Condition* cond){
	pthread_cond_destroy(cond->cond);
	free(cond->cond);
	free(cond);
}

struct EWFP_Thread_Semaphore{
	sem_t*	sem;
	unsigned int maxValue;
};
EWFP_Thread_Semaphore* EWFP_Thread_Semaphore_New(unsigned int initialValue,unsigned int maxValue){
	EWFP_Thread_Semaphore* s=calloc(1,sizeof(*s));
	s->maxValue=maxValue; //stored, but unsupported for now
	s->sem=calloc(1,sizeof(sem_t));
	sem_init(s->sem,0,initialValue);
	return s;
}
int EWFP_Thread_Semaphore_Post(EWFP_Thread_Semaphore* sem){
	return sem_post(sem->sem);
}
int EWFP_Thread_Semaphore_Wait(EWFP_Thread_Semaphore* sem,unsigned int timeout){
	if(timeout==0)
		return pthread_sem_wait(sem->sem);
	else{
		timespec t;
		clock_gettime(CLOCK_REALTIME,&t);
		t.nanoseconds+=(timeout*1000000);
		return pthread_sem_timedwait(sem->sem,&t);
	}
}
int EWFP_Thread_Semaphore_WaitNoBlock(EWFP_Thread_Semaphore* sem){
	return sem_trywait(sem->sem);
}
void EWFP_Thread_Semaphore_Free(EWFP_Thread_Semaphore* sem){
	sem_destroy(sem->sem);
	free(sem->sem);
	free(sem);
}

struct EWFP_Thread_Thread{
	pthread_t*	thread;
	int		started;
	void*		data;
	void*(*fxn)(void*);
};
void* _EWFP_Thread_Thread_helper(void* data){
	EWFP_Thread_Thread* thread=(EWFP_Thread_Thread*)data;
	while(thread->started);
	return t->fxn(t->data);
}
EWFP_Thread_Thread* EWFP_Thread_Thread_New(int (*fxn)(void*),void* data){
	EWFP_Thread_Thread* t=calloc(1,sizeof(*t));
	t->thread=calloc(1,sizeof(pthread_t));
	t->started=0;
	t->data=data;
	t->fxn=fxn;
	return t;
}
int EWFP_Thread_Thread_Start(EWFP_Thread_Thread* thread){
	if(!thread->started)
		thread->started=1;
	return 0;
}
int EWFP_Thread_Thread_Kill(EWFP_Thread_Thread* thread){
	return pthread_cancel(*(thread->thread));
}
int EWFP_Thread_Thread_Join(EWFP_Thread_Thread* thread){
	pthread_join(*(thread->thread),NULL);
	return 0;
}
unsigned int EWFP_Thread_Thread_GetID(EWFP_Thread_Thread* thread){
	return 0; //not supported by pthreads
}
void EWFP_Thread_Thread_Free(EWFP_Thread_Thread* thread){
	EWFP_Thread_Thread_Kill(thread);
	free(thread->thread);
	free(thread);
}
void EWFP_Thread_Thread_Sleep(unsigned int ms){
	if(ms>1000)
		sleep(ms/1000);
	usleep((ms-ms/1000)*1000);
}
