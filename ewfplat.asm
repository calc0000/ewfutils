;typedef struct EWFP_DSO_Call_Arg{
;	unsigned int argSize;
;	void* arg;
;}EWFP_DSO_Call_Arg;
;extern int				EWFP_DSO_Call_cdecl		(void* fxn,unsigned int retSize,void* ret,unsigned int argCount,EWF_DSO_Call_Arg* args);
;													ebp+8
global _EWFP_DSO_Call_cdecl
section .text
_EWFP_DSO_Call_cdecl:
	;housekeeping
	push	ebp
	mov		ebp,esp

	cmp		dword [ebp+8],0	;if fxn is NULL, do nothing
	je		.error

	mov		ecx,[ebp+20]	;fetch argCount
.argloop:
	dec		ecx
	cmp		ecx,0
	jg		.argloop

	jmp		.end

.error:
	mov		eax,-1		;return -1 on error

.end:
	ret
