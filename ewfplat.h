#ifndef EWF_PLATFORM_H
#define EWF_PLATFORM_H

#include <stdlib.h>

#ifdef __cplusplus
extern "C"{
#endif

//lightweight stuff!

void				EWFP_urandom		(void* buffer,unsigned int size);
unsigned int		EWFP_GetTicks		();

unsigned int		EWFP_Sys_GetPageSize	();

extern const int			EWFP_VALLOC_BIGPAGE	;
extern const int			EWFP_VALLOC_PAGE_x	;
extern const int			EWFP_VALLOC_PAGE_xr	;
extern const int			EWFP_VALLOC_PAGE_xrw;
extern const int			EWFP_VALLOC_PAGE_r	;
extern const int			EWFP_VALLOC_PAGE_rw	;
extern const int			EWFP_VALLOC_PAGE_w	;
void*				EWFP_valloc			(void* start,size_t size,unsigned int flags);
void				EWFP_vfree			(void* chunk);

struct	EWFP_Mutex;
typedef struct EWFP_Mutex EWFP_Mutex;
EWFP_Mutex*			EWFP_Mutex_New		();
int					EWFP_Mutex_Lock		(EWFP_Mutex* mutex); //0 for no timeout
int					EWFP_Mutex_Unlock	(EWFP_Mutex* mutex);
void				EWFP_Mutex_Free		(EWFP_Mutex* mutex);

struct	EWFP_Condition;
typedef struct EWFP_Condition EWFP_Condition;
EWFP_Condition*			EWFP_Condition_New			();
int						EWFP_Condition_Wait			(EWFP_Condition* cond,EWFP_Mutex* mutex,unsigned int timeout);	//0 for no timeout
int						EWFP_Condition_Signal		(EWFP_Condition* cond);
int						EWFP_Condition_Broadcast	(EWFP_Condition* cond);
void					EWFP_Condition_Free			(EWFP_Condition* cond);

struct	EWFP_Semaphore;
typedef struct EWFP_Semaphore EWFP_Semaphore;
EWFP_Semaphore*			EWFP_Semaphore_New			(unsigned int initialValue,unsigned int maxValue);
int						EWFP_Semaphore_Post			(EWFP_Semaphore* sem);
int						EWFP_Semaphore_Wait			(EWFP_Semaphore* sem,unsigned int timeout); //0 for no timeout
int						EWFP_Semaphore_WaitNoBlock	(EWFP_Semaphore* sem);
void					EWFP_Semaphore_Free			(EWFP_Semaphore* sem);

typedef enum EWFP_Thread_State{
	EWFP_THREAD_STATE_RUNNING,
	EWFP_THREAD_STATE_SUSPENDED,
	EWFP_THREAD_STATE_TERMINATED,
}EWFP_Thread_State;
struct	EWFP_Thread;
typedef struct EWFP_Thread EWFP_Thread;
EWFP_Thread*			EWFP_Thread_New			(int (*fxn)(void*),void* data);
int						EWFP_Thread_Start		(EWFP_Thread* thread);
int						EWFP_Thread_Kill		(EWFP_Thread* thread);
int						EWFP_Thread_Join		(EWFP_Thread* thread);
unsigned int			EWFP_Thread_GetID		(EWFP_Thread* thread);
void					EWFP_Thread_Free		(EWFP_Thread* thread);
void					EWFP_Thread_Sleep		(unsigned int ms);
EWFP_Thread_State		EWFP_Thread_GetState	(EWFP_Thread* thread);

struct	EWFP_DSO;
typedef struct EWFP_DSO EWFP_DSO;
EWFP_DSO*				EWFP_DSO_Open			(const char* filename);
void*					EWFP_DSO_GetAddress		(EWFP_DSO* dso,const char* name);
typedef struct EWFP_DSO_Call_Arg{
	unsigned int argSize;
	void* arg;
}EWFP_DSO_Call_Arg;
extern int				EWFP_DSO_Call_cdecl		(void* fxn,unsigned int retSize,void* ret,unsigned int argCount,EWFP_DSO_Call_Arg* args);	//TODO: Implement
void					EWFP_DSO_Close			(EWFP_DSO* dso);

#ifdef __cplusplus
}
#endif

#endif