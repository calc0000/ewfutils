#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include "ewfplat.h"
#include "ewfutils.h"

unsigned char* memDblResize(EWF_MEMFILE* mf,int numberOfTimes){
	mf->capacity*=(long)pow(2.0f,numberOfTimes);
	mf->buffer=(unsigned char*)realloc(mf->buffer,mf->capacity);
	return mf->buffer;
}

void usage(int argc,char* argv[]){
	printf("Usage: %s\n",argv[0]);
}

int main(int argc,char* argv[]){
	//main_bt(argc,argv);
	//main_bf(argc,argv);
	//main_ll(argc,argv);
	//main_opt(argc,argv);
	//main_log(argc,argv);
	//main_random(argc,argv);
	main_timer(argc,argv);
	system("pause");
	return 0;
}

int main_timer(int argc,char* argv[]){
	int i=0;
	for(;i<100;i++){
		printf("%d\n",EWFP_GetTicks());
		EWFP_Thread_Sleep(50);
	}
	return 0;
}

int main_random(int argc,char* argv[]){
	unsigned int num=0;
	unsigned int i=0;
	for(;i<10;i++){
		EWFP_urandom(&num,sizeof(num));
		printf("%d\n",num);
	}
	return 0;
}

int main_bt(int argc,char* argv[]){
	//random binary tree balance test
	unsigned int numToInsert=100000;
	unsigned int i=0;
	EWF_BTree* bt=EWF_BTree_New();
	srand(1);
	for(;i<numToInsert;i++){
		int key=rand();
		int val=rand();
		EWF_BTree_Node* n=EWF_BTree_Node_New((void*)key,(void*)val);
		EWF_BTree_Insert(bt,n);
	}
	{
		clock_t start=clock();
		EWF_BTree_Balance(bt);
		printf("Took %d ticks to balance %d entries.\n",clock()-start,numToInsert);
	}
	system("pause");
	return 0;
}

int main_bf(int argc,char* argv[]){
	EWF_MEMFILE* mf=mopen("test.ewfbf","wb");
	EWF_Block_File* bf=EWF_Block_File_Open(mf);
	EWF_Block_File_Comment* c1=calloc(1,sizeof(*c1));
	{
		EWF_Block_Block* b=EWF_Block_Block_New();
		c1->name=calloc(1,strlen("comment")+1);
		strncpy(c1->name,"comment",strlen("comment"));
		c1->comment=calloc(1,strlen("this is a sentence")+1);
		strncpy(c1->comment,"this is a sentence",strlen("this is a sentence"));
		b->file=bf;
		b->type=0x02;
		b->size=strlen("comment")+strlen("this is a sentence")+2;
		b->data=c1;
		bf->blocks=EWF_LL_Attach(bf->blocks,EWF_LL_New(b));
		bf->blockCount++;
	}
	EWF_Block_File_Write(bf);
	mf->backend.control=1;
	mclose(mf);
	{
		EWF_Block_File* bf2=EWF_Block_File_Open(mopen("test.ewfbf","rb"));
		EWF_Block_File_Parse(bf2);
		bf2=bf2;
	}
	system("pause");
	return 0;
}

int main_ll(int argc,char* argv[]){
	EWF_LL* ll=NULL;
	EWF_Queue* q=EWF_Queue_New(0);
	clock_t start=clock();
	clock_t timeTaken=0;
	unsigned int i=0;
	unsigned int l=1000000;

	start=clock();
	for(i=0;i<l;i++){
		EWF_LL* attach=EWF_LL_New((void*)i);
		ll=EWF_LL_Attach(ll,attach);
	}
	timeTaken=clock()-start;
	printf("Time for %d attaches: %d ticks (%d)\n",l,timeTaken,CLOCKS_PER_SEC);

	start=clock();
	for(i=0;i<l;i++){
		EWF_Queue_Push_block(q,(void*)i);
	}
	timeTaken=clock()-start;
	printf("Time for %d pushes: %d ticks (%d)\n",l,timeTaken,CLOCKS_PER_SEC);

	start=clock();
	for(i=0;i<l;i++){
		int c=(int)EWF_Queue_Pop_block(q);
	}
	timeTaken=clock()-start;
	printf("Time for %d pops: %d ticks (%d)\n",l,timeTaken,CLOCKS_PER_SEC);

	start=clock();
	for(i=0;i<l;i++){
		EWF_Queue_Push_noblock(q,(void*)i);
	}
	timeTaken=clock()-start;
	printf("Time for %d noblock pushes: %d ticks (%d)\n",l,timeTaken,CLOCKS_PER_SEC);

	start=clock();
	for(i=0;i<l;i++){
		EWF_Queue_Pop_noblock(q);
	}
	timeTaken=clock()-start;
	printf("Time for %d noblock pops: %d ticks (%d)\n",l,timeTaken,CLOCKS_PER_SEC);

	system("pause");
	return 0;
}

int main_opt(int argc,char* argv[]){
	char* buf;
	char c=0;
	printf("parsing options:\n");
	while((c=EWF_getopt(argc,argv,"-a:b::cd",(char**)&buf,NULL,NULL,NULL))!=-1){
		printf("opt: %d\n",c);
		printf("\targ: %s\n",buf);
	}
	return 0;
}

int main_log(int argc,char* argv[]){
	EWF_INI_File* ret=NULL;
	FILE* fp=fopen("cfg.ini","rb");
	EWF_MEMFILE* mf=mlink(mnew(),fopen("testout.txt","wb"));
	mf->backend.control=1;
	ret=EWF_INI_Parse(mlink(mnew(),fp),1,1);
	printf("val: %s\n",EWF_INI_Get(ret,"section1.sub1.prop3"));
	EWF_INI_Free(ret,1);
	fclose(fp);
	{
		EWF_Logger* log=lnew("hai");
		printf("Holding log.\n");
		EWF_Logger_Hold(log);
		mprintf(mf,"The following line is a Logger output: (%d)\n",2);
		ladd(log,mf);
		llog_ex(log,L_L_INFO,"wassup %d brohead",1337);
		llog_ex(log,L_L_CRIT,"oh teh noez! %g",1.456);
		printf("Releasing log.\n");
		EWF_Logger_Release(log);
		lfree(log);
	}
	mclose(mf);
	{
		EWF_Queue* q=EWF_Queue_New(0);
		EWF_Queue_Push_block(q,(void*)1);
		EWF_Queue_Push_block(q,(void*)2);
		EWF_Queue_Push_block(q,(void*)3);
		printf("First: %d\n",(int)EWF_Queue_Pop_noblock(q));
		printf("2nd: %d\n",(int)EWF_Queue_Pop_noblock(q));
		EWF_Queue_Free(q);
	}
	system("pause");
	return 0;
}