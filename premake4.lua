local NAME		="ewfutils"
local LANGUAGE		="C"
local INCLIB_ROOT	= "inclib/"

local libs={
		}

solution (NAME)

	location "./proj"
	configurations {"Debug","Release"}
	includedirs {"include"}
	platforms {"x32","x64"}

	configuration "x32"
		targetsuffix "_x86"
	configuration "x64"
		targetsuffix "_x64"
	configuration "Debug"
		defines { "DEBUG", "_DEBUG", "_STLP_DEBUG" }
		flags { "Symbols" }
		targetdir "bin/debug"
		objdir "obj/debug"
	configuration "Release"
		defines { "NDEBUG", "_NDEBUG" }
		flags { "OptimizeSpeed" }
		targetdir "bin/release"
		objdir "obj/release"
	configuration {"windows"}
		defines {"WIN32","_WIN32","_WINDOWS"}
		excludes {"*pthread*"}
	if not os.get()=="windows" then
		excludes {"*win*"}
	end

	project(NAME.."test")
		kind "ConsoleApp"
		language "C"
		files {"ewfutilstest.c"}

		links {(NAME)}
	
    project(NAME .. "pp")
        kind "StaticLib"
        language ("C++")
        files {"pp/*.cpp","pp/*.h"}

        includedirs{
            (INCLIB_ROOT),
        }
        libdirs{
            (INCLIB_ROOT),
        }
	project(NAME)
		kind "StaticLib"
		language (LANGUAGE)
		files {"*.c","*.h"}
		excludes {"ewfutilstest.c"}
		
		includedirs{
			(INCLIB_ROOT),
		}
		libdirs{
			(INCLIB_ROOT),
		}
