#ifndef EWF_UTILS_H 
#define EWF_UTILS_H

#ifdef __cplusplus
extern "C"{
#endif

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

#include "ewfplat.h"

//	EWF_MapFxn(container,element,upvalue);
typedef int(*EWF_MapFxn)	(void*,void*,void*);

/*EWF_Hash*/
void	EWF_Hash_Murmur_x86_32	(const void* key,int len,unsigned int seed,void* out);

/*==================================*/
/*EWF_MEMFILE*/
/*
* EWF_MEMFILE intends to be a binary-compatible replacement for the FILE* api.
* To accomplish this, the following functions have been implemented for
* EWF_MEMFILE's:
* 
* 	mclose		->	fclose
* 	mopen		->	fopen
* 	mgets		->	fgets
* 	mread		->	fread
* 	mwrite		->	fwrite
* 	mprintf		->	fprintf
* 	mtell		->	ftell
* 	mseek		->	fseek
* 
* These have expected behavior both when used with a buffer backend, or a user-
* provided backend.
* 
* MEMORY OWNERSHIP:
*
*   ewfutils maintains ownership of the EWF_MEMFILE struct through the
*   mnew/mclose functions.
*     mnew/mclose -> malloc/free
*
*   ewfutils maintains ownership of the EWF_MEMFILE->buffer field, when it
*   exists, through the mnew/mclose functions.
*     mnew/mclose -> malloc/free
*
* THREAD SAFETY:
* 
*   The above FILE* replacement functions are only as thread safe as the
*   underlying CRT functions, when EWF_MEMFILE is being used with a FILE*
*   backend.  When EWF_MEMFILE is being used with a backing memory buffer,
*   caller is responsible for enforcing thread safety.
*
*   All other functions are NOT thread safe.  Caller is responsible for
*   enforcing thread safety for these functions.
*/
#define	MSEEK_SET	0
#define	MSEEK_CUR	1
#define	MSEEK_END	2
typedef struct struct_EWF_MEMFILE{
	struct{
		char use;
		char control;
		void* closure;
		size_t	(*read)	(void*,size_t,size_t,void*);
		size_t	(*write)(const void*,size_t,size_t,void*);
		int		(*close)(void*);
		char*	(*gets)	(char*,int,void*);
		long	(*tell)	(void*);
		int		(*seek)	(void*,long int,int);
	}backend;
	unsigned char*	buffer;
	size_t			size;
	size_t			capacity;
	size_t			readIndex;
	size_t			writeIndex;
	unsigned char*	(*resizeFxn)(struct struct_EWF_MEMFILE*,int);
}EWF_MEMFILE;

extern const int	__EWF_MEMFILE_CHUNKSIZE;
EWF_MEMFILE*		mnew	();
int					mclose	(EWF_MEMFILE* stream);
EWF_MEMFILE*		mopen	(const char* filename,const char* mode);
char*				mgets	(char* str,int num,EWF_MEMFILE* stream);
size_t				mread	(void* dstbuf,size_t size,size_t count,EWF_MEMFILE* stream);
size_t				mwrite	(const void* dstbuf,size_t size,size_t count,EWF_MEMFILE* stream);
int					mprintf	(EWF_MEMFILE* stream,char* format,...);
size_t				mtell	(EWF_MEMFILE* stream);
size_t				mtellw	(EWF_MEMFILE* stream);
size_t				mtellr	(EWF_MEMFILE* stream);
int					mseek	(EWF_MEMFILE* stream,long int offset,int origin);
int					mseekw	(EWF_MEMFILE* stream,long int offset,int origin);
int					mseekr	(EWF_MEMFILE* stream,long int offset,int origin);
void				mtrim	(EWF_MEMFILE* stream);
void				mcap	(EWF_MEMFILE* stream,long int capacity);
unsigned char*		mgetptr	(EWF_MEMFILE* stream);
unsigned char*		mgetptrr(EWF_MEMFILE* stream);
unsigned char*		mgetptrw(EWF_MEMFILE* stream);
unsigned char*		mresize	(EWF_MEMFILE* stream,int numberOfTimes);
EWF_MEMFILE*		mlink	(EWF_MEMFILE* mf,FILE* f);
int					mlinked	(EWF_MEMFILE* mf);
#define MEMFILE EWF_MEMFILE

/*==================================*/
/*_EWF_LL*/
/*
* EWF_LL is an implementation of a simple doubly linked list, with the following
* additional characteristics:
*
*   head->prev = tail;
*   tail->next = NULL;
*
* MEMORY OWNERSHIP:
*
*   ewfutils allocates EWF_LL structs, but caller is responsible for freeing using free.
*     EWF_LL_New/[caller] -> calloc/free
*
*   Caller maintains ownership of the void* data pointer.
*
* THREAD SAFETY:
*
*   None of the EWF_LL functions are thread safe.  Caller is responsible for
*   enforcing thread safety.
*/
typedef struct struct_EWF_LL{
	struct struct_EWF_LL*		prev;
	struct struct_EWF_LL*		next;
	void*		data;
}EWF_LL;

EWF_LL*	EWF_LL_New			(void* data);
int		EWF_LL_GetSize		(EWF_LL* head);
//returns the new head
EWF_LL*	EWF_LL_Orphan		(EWF_LL* head,EWF_LL* ll);
//returns:	(head?head:attach)
EWF_LL*	EWF_LL_Attach		(EWF_LL* head,EWF_LL* attach);
//call map(NULL,ll,upvalue) for every ll on head
void	EWF_LL_Map			(EWF_LL* head,EWF_MapFxn map,void* upvalue);
//calls free(((EWF_LL*)vll)->data), then free((EWF_LL*)vll)
int		EWF_LL_DefaultFree	(void* unused,void* vll,void* upvalue);
/*==================================*/
/*EWF_Array*/
struct	EWF_Array;
typedef struct EWF_Array EWF_Array;
//														cap				count
typedef unsigned int(*EWF_Array_ResizeFxn)(EWF_Array*,unsigned int,unsigned int);
struct EWF_Array{
	unsigned int		count;
	unsigned int		cap;
	EWF_Array_ResizeFxn	resizeFxn;
	void**				data;
};

EWF_Array*	EWF_Array_New	(unsigned int cap,EWF_Array_ResizeFxn resizeFxn);
void**		EWF_Array_Push	(EWF_Array* arr,void* data);
void**		EWF_Array_Index	(EWF_Array* arr,unsigned int idx);
void		EWF_Array_Free	(EWF_Array* arr);
/*==================================*/
/*Queue*/
/*
* EWF_Queue implements a First Out Last In (FILO) structure, built on top of a
* linked list, with some thread safety capabilities.
*
* MEMORY OWNERSHIP:
*
*   ewfutils maintains ownership of the EWF_Queue struct through the
*   EWF_Queue_New and EWF_Queue_Free functions.
*     EWF_Queue_New/EWF_Queue_Free -> calloc/free
*
*   ewfutils maintains ownership of the linked list at EWF_Queue->top through
*   the EWF_Queue_Push* and EWF_Queue_Free functions.
*     EWF_Queue_Push/EWF_Queue_Free -> EWF_LL_New/free
*
*   ewfutils maintains ownership of the EWF_Queue->_mutex and
*   EWF_Queue->_condition fields through the EWF_Queue_New and EWF_Queue_Free
*   functions.
*     EWF_Queue_New/EWF_Queue_Free -> EWFP_Mutex_New/EWFP_Mutex_Free
*     EWF_Queue_New/EWF_Queue_Free -> EWFP_Condition_New/EWFP_Condition_Free
*
*   Caller maintains ownership of all void* data inserted into the queue by
*   EWF_Queue_Push*.
*
* THREAD SAFETY:
*
*   The following functions are thread safe in the expected way:
*     EWF_Queue_Push_block
*     EWF_Queue_Push_noblock
*     EWF_Queue_Pop_block
*     EWF_Queue_Pop_noblock
*     EWF_Queue_Peek
*   All other EWF_Queue functions are not thread safe.  Caller must enforce
*   appropriate thread safety.
*
*/
typedef struct _struct_EWF_Queue{
	EWF_LL*					top;
	unsigned int			_count;
	unsigned int			capacity;
	EWFP_Mutex*		_mutex;
	EWFP_Condition*	_cond;
}EWF_Queue;

//0 for unlimited
EWF_Queue*	EWF_Queue_New			(unsigned int capacity);
EWF_Queue*	EWF_Queue_Combine		(EWF_Queue* receiver,EWF_Queue* sacrifice);
//returns p if q is full, NULL if q is not empty
void*		EWF_Queue_Push_block	(EWF_Queue* q,void* data);
void*		EWF_Queue_Push_noblock	(EWF_Queue* q,void* data);
//returns NULL if q is empty, p if q is not empty
void*		EWF_Queue_Pop_block		(EWF_Queue* q);
void*		EWF_Queue_Pop_noblock	(EWF_Queue* q);
void*		EWF_Queue_Peek			(EWF_Queue* q);
void		EWF_Queue_Free			(EWF_Queue* q);
unsigned int	EWF_Queue_Count		(EWF_Queue* q);
//call EWF_LL_Map(head->top,map,upvalue);
void		EWF_Queue_Map			(EWF_Queue* head,EWF_MapFxn map,void* upvalue);
/*==================================*/
/*Logger*/
/*
* EWF_Logger implements a (mostly) thread safe, multi-destination application
* logger.  There are two formats for the output:
*
*   Standard output:
*     logName[messageLevel]: message\n
*   Extended output:
*     logName[messageLevel](functionName@lineNumber): message\n
*
* EWF_Logger provides the capability to "save" messages for future output to
* destinations.
*
* Destinations for EWF_Logger are provided by EWF_MEMFILE's.  This allows for
* output to in-memory buffers, normal FILE*'s, or any other output that can be
* expressed in EWF_MEMFILE->backend.
*
* MEMORY OWNERSHIP:
*
*   ewfutils maintains ownership of the EWF_Logger struct through the
*   EWF_Logger_New and EWF_Logger_Free functions.
*     EWF_Logger_New/EWF_Logger_Free -> malloc/free
*
*   ewfutils maintains ownership of the EWF_Logger->name field through the
*   EWF_Logger_New and EWF_Logger_Free functions.
*     EWF_Logger_New/EWF_Logger_Free -> strcpy/free
*
*   ewfutils maintains ownership of the linked list of destinations through the
*   EWF_Logger_AddDestination and EWF_Logger_Free functions.
*     EWF_Logger_AddDestination/EWF_Logger_Free -> EWF_LL_New/free
*
*   Caller maintains ownership of the actual destination EWF_MEMFILE's contained
*   in EWF_Logger->destinations.
*
*   ewfutils maintains ownership of the EWF_Logger->messageQueue field through
*   the EWF_Logger_New and EWF_Logger_Free functions.
*     EWF_Logger_New/EWF_Logger_Free -> EWF_Queue_New/EWF_Queue_Free
*   
*   ewfutils maintains ownership of the actual message strings generated by
*   EWF_Logger_Log and EWF_Logger_Log_Ex (and currently leaks it if the logger
*   is freed when non-empty).
*     EWF_Logger_Log/EWF_Logger_Free -> malloc/free
*
* THREAD SAFETY:
*
*   If the logger is saving messages, the following functions are as thread safe
*   as EWF_Queue_Push:
*     EWF_Logger_Log
*     EWF_Logger_Log_Ex
*     EWF_Logger_Flush
*
*   When the logger is NOT saving messages, the following functions are as
*   thread safe as the underlying EWF_MEMFILE destination:
*     EWF_Logger_Log
*     EWF_Logger_Log_Ex
*     EWF_Logger_Flush
*
*   All other functions are not thread safe.  Caller is responsible for
*   enforcing appropriate thread safety.
*
*/
#ifndef EWF_LOGGER_MAX_LOG_LENGTH
#define EWF_LOGGER_MAX_LOG_LENGTH 2048
#endif

#ifndef EWF_LOGGER_NO_SHORT_NAMES
#define lnew	EWF_Logger_New
#define lfree	EWF_Logger_Free
#define ladd	EWF_Logger_AddDestination
#define llog	EWF_Logger_Log
#ifdef _MSC_VER
#define llog_ex(log,level,format,...) EWF_Logger_Log_Ex(log,level,__FUNCTION__,__LINE__,format,__VA_ARGS__)
#else
#define llog_ex(log,level,format,z...) EWF_Logger_Log_Ex(log,level,__FUNCTION__,__LINE__,format,##z)
#endif
#define lfilter	_Logger_FilterOut
#define L_L_WARN	EWF_LOGGER_LEVEL_WARNING
#define L_L_INFO	EWF_LOGGER_LEVEL_INFO
#define L_L_ERR		EWF_LOGGER_LEVEL_ERROR
#define L_L_CRIT	EWF_LOGGER_LEVEL_CRIT
#endif

//levels
#define	EWF_LOGGER_LEVEL_WARNING	"WARN"
#define EWF_LOGGER_LEVEL_INFO	"INFO"
#define EWF_LOGGER_LEVEL_ERROR	"ERR"
#define EWF_LOGGER_LEVEL_CRIT	"CRIT"

typedef struct struct_Logger{
	EWF_LL*		destinations;	//linked list of MEMFILE*'s
	char*		name;		//prefix for messages
	char		filter;
	EWF_Queue*	messageQueue;
	int			savingMessages;
}EWF_Logger;


EWF_MEMFILE*	EWF_Logger_AddDestination	(EWF_Logger* log,EWF_MEMFILE* dest);
void			EWF_Logger_FilterOut		(EWF_Logger* log,const char* level);
EWF_Logger*		EWF_Logger_New				(const char* name);
void			EWF_Logger_Free				(EWF_Logger* log);
void			EWF_Logger_Hold				(EWF_Logger* log);
void			EWF_Logger_Release			(EWF_Logger* log);
void			EWF_Logger_Flush			(EWF_Logger* log);
void			EWF_Logger_Log				(EWF_Logger* log,const char* level,const char* format,...);
void			EWF_Logger_Log_Ex			(EWF_Logger* log,const char* level,const char* fxn,int line,const char* format,...);
//call EWF_LL_Map(log->destinations,map,upvalue);
void			EWF_Logger_MapDestinations	(EWF_Logger* log,EWF_MapFxn map,void* upvalue);
//calls mclose((EWF_MEMFILE*)((EWF_LL*)vll)->data), then free(vll)
int				EWF_Logger_DefaultDestinationMap	(void* log,void* vll,void* upvalue);
//call EWF_Queue_Map(log->messageQueue,map,upvalue);
void			EWF_Logger_MapMessages		(EWF_Logger* log,EWF_MapFxn map,void* upvalue);
/*=======================*/
/*POSIX getopt,getopt_long,getopt_long_only implementation*/
/*WITH GNU extensions*/
/*
* EWF_getopt implements a POSIX-compliant getopt and getopt_long_only (with GNU
* extensions).  Additionally, optarg, optind, opterr, and optopt are passed as
* arguments to the function to remove some of the standard implementation's
* global variables.
*
* EWF_getopt recognizes single-character option switches, denoted by the '-'
* character, where the option's argument (when it exists), can be part of the
* switch, or the next argv: 
*   -oarg 
*   -o arg
*
* EWF_getopt_long_only recognizes both single-character and string option
* switches, where the single character switch is denoted by '-', and string
* option switches are denoted by either '-' or '--'.  Single character option
* switches take precedence in parsing.  Option arguments can be expressed in the
* following ways:
*   -oarg
*   -o arg
*   -option=arg
*   -option arg
*   --option=arg
*   --option arg
*
* See glibc man page for getopt for additional details.
*
* NOTE:
*
*   This implementation of getopt does not currently work with multiple
*   instances.  One command line must finish parsing before another can be
*   parsed.
*
* MEMORY OWNERSHIP:
*
*   Caller is responsible for optstring, argv, and longopts (where applicable).
*
*   Returned optarg's are pointers from argv.  Thus, caller is responsible for
*   optarg's, to the extent that caller is responsible for argv.
*
* THREAD SAFETY:
*
*   None of the EWF_getopt functions are thread safe.  Additionally, multiple
*   instances of EWF_getopt is not currently supported.  Caller is responsible
*   for obeying these conditions.
*/

typedef struct struct_option{
	const char*	name;
	int			has_arg;
	int*		flag;
	int			val;
}EWF_option;

extern const int	EWF_getopt_no_argument		;
extern const int	EWF_getopt_required_argument;
extern const int	EWF_getopt_optional_argument;

int	EWF_getopt			(int argc,char* argv[],const char* optstring,
							char** optarg,int* optind,int* opterr,int* optopt);

int	EWF_getopt_long		(int argc,char* argv[],const char* optstring,
							const EWF_option* longopts,int* longindex,
							char** optarg,int* optind,int* opterr,int* optopt);

int	EWF_getopt_long_only	(int argc,char* argv[],const char* optstring,
								const EWF_option* longopts,int* longindex,
								char** optarg,int* optind,int* opterr,int* optopt);

#ifdef EWF_EMULATE_POSIX
typedef	EWF_option	option;
const int		no_argument=0;
const int		required_argument=1;
const int		optional_argument=2;
extern char*	optarg;
extern int		optind,opterr,optopt;
#define	getopt(a,b,c)				EWF_getopt(a,b,c,&optarg,&optind,&opterr,&optopt)
#define	getopt_long(a,b,c,d,e)		EWF_getopt_long(a,b,c,d,e,&optarg,&optind,&opterr,&optopt)
#define getopt_long_only(a,b,c,d,e)	EWF_getopt_long_only(a,b,c,d,e,&optarg,&optind,&opterr,&optopt)
#endif

/*==================================*/
/*Binary Tree*/
/*
* EWF_BTree implements a basic binary search tree.  Key comparison is performed
* by an EWF_BTree_CompareFxn stored in EWF_BTree->compareKeys.  This function
* must behave as follows:
*                                           | <0, if x < y
*   EWF_BTre->compareKeys(x,y) must return: |  0, if x == y
*                                           | >0, if x > y
*
* MEMORY OWNERSHIP:
*
*   ewfutils maintains ownership of the EWF_BTree structure through the
*   EWF_BTree_New and EWF_BTree_Free functions.
*     EWF_BTree_New/EWF_BTree_Free -> calloc/free
*
*   ewfutils maintains ownership of the EWF_BTree_Node structure through the
*   EWF_BTree_Node_New and EWF_BTree_Node_Free functions.
*     EWF_BTree_Node_New/EWF_BTree_Node_Free -> calloc/free
*
*   Caller is responsible for EWF_BTree_Node->key and EWF_BTree_Node->value,
*   where applicable.
*
* THREAD SAFETY:
*
*   None of the EWF_BTree functions are thread safe.  Caller is responsible for
*   enforcing appropriate thread safety.
*/
typedef int(*EWF_BTree_CompareFxn)(void*,void*);
typedef struct EWF_BTree_Node{
	struct EWF_BTree_Node*	parent;
	struct EWF_BTree_Node*	left;
	struct EWF_BTree_Node*	right;
	void*					key;
	void*					value;
}EWF_BTree_Node;
EWF_BTree_Node*	EWF_BTree_Node_New	(void* key,void* value);
void			EWF_BTree_Node_Free	(EWF_BTree_Node* n);
typedef struct EWF_BTree{
	EWF_BTree_Node*	root;
	int	dirty;
	int	balanceOnInsert;		//maintain balance (or not)
	EWF_BTree_CompareFxn compareKeys;	//return: {x<y:<0,x=y:0,x>y:>0}
}EWF_BTree;
EWF_BTree*		EWF_BTree_New		();
EWF_BTree_Node*	EWF_BTree_Insert	(EWF_BTree* bt,EWF_BTree_Node* n);
//returns the orphaned node
EWF_BTree_Node*	EWF_BTree_Remove	(EWF_BTree* bt,EWF_BTree_Node* n);
EWF_BTree_Node*	EWF_BTree_Lookup	(EWF_BTree* bt,void* key);
void			EWF_BTree_Balance	(EWF_BTree* bt);
void			EWF_BTree_Free		(EWF_BTree* bt);
//calls map(bt,node,upvalue) for every node in bt
void			EWF_BTree_Map		(EWF_BTree* bt,EWF_MapFxn map,void* upvalue);

//BTree specialized for char* keys
EWF_BTree*		EWF_Dict_New		();
typedef	EWF_BTree	EWF_Dict;
/*=======================*/
/*EWF_HashTable*/
/*
* EWF_HashTable implements a hash table built on top of the EWF_BTree structure.
* The hash function currently used is MurmurHash3, implemented by the
* EWF_Hash_Murmur* functions.
*
* MEMORY OWNERSHIP:
*
*   ewfutils maintains ownership of the EWF_Hashtable structure through the
*   EWF_HashTable_New/EWF_HashTable_Free functions.
*     EWF_HashTable_New/EWF_HashTable_Free -> EWF_BTree_New/EWF_BTree_Free
*
*   Caller maintains ownership of the void* values inserted into the hash table.
*
* THREAD SAFETY:
*
*   EWF_HashTable is only as thread safe as EWF_BTree (currently, that is not at
*   all).  Thus, caller is responsible for enforcing appropriate thread safety.
*
*/
typedef EWF_BTree	EWF_HashTable;

EWF_HashTable*	EWF_HashTable_New		();
void*			EWF_HashTable_Insert	(EWF_HashTable* ht,void* value,void* key,size_t keylen);
void*			EWF_HashTable_Lookup_k	(EWF_HashTable* ht,void* key,size_t keylen);
void*			EWF_HashTable_Lookup_h	(EWF_HashTable* ht,unsigned int hash);
void			EWF_HashTable_Free		(EWF_HashTable* ht);
//calls EWF_BTree_Map(ht,map,upvalue);
void			EWF_HashTable_Map		(EWF_HashTable* ht,EWF_MapFxn map,void* upvalue);
/*=======================*/
/*_Dict*/
typedef struct struct_EWF_ClassicDict{
	char**	keys;
	void**	values;
	int		_size;
	int		_capacity;
}EWF_ClassicDict;

EWF_ClassicDict*	EWF_ClassicDict_New		(int startingCap);
int			EWF_ClassicDict_Expand		(EWF_ClassicDict* dict);
int			EWF_ClassicDict_GetIndexOf	(EWF_ClassicDict* dict,char* key);
void*		EWF_ClassicDict_Update		(EWF_ClassicDict* dict,char* key,void* newVal);
void*		EWF_ClassicDict_Insert		(EWF_ClassicDict* dict,char* key,void* value);
void*		EWF_ClassicDict_Get_k		(EWF_ClassicDict* dict,char* key);
void*		EWF_ClassicDict_Get_i		(EWF_ClassicDict* dict,int index);
char*		EWF_ClassicDict_Delete		(EWF_ClassicDict* dict,char* key);
void		EWF_ClassicDict_Free		(EWF_ClassicDict* dict,int freeValuesByLibC);

#define _EWF_ClassicDict EWF_ClassicDict
/*==================================*/
/*Block Data File*/
/*
File Structure:
	Magic:			EWFBF
	Block Count:	4 bytes
	Blocks...
Reserved Block Types:
	0x00:	Schema Definition
	0x01:	File Metadata (last one takes effect)
	0x02:	Comment
*/

typedef void*(*EWF_Block_Parser)(unsigned short,	//type
	unsigned char,									//options
	unsigned int,									//size
	EWF_MEMFILE*,									//stream
	unsigned int*,									//szOut
	void*);											//closure
typedef void(*EWF_Block_Writer)(unsigned short,		//type
	unsigned char,									//options
	void*,											//data
	unsigned int,									//size
	EWF_MEMFILE*,									//stream
	unsigned int*,									//szOut
	void*);											//closure
typedef struct EWF_Block_Block_Defn{
	void*				pData;
	EWF_Block_Parser	p;
	void*				wData;
	EWF_Block_Writer	w;
}EWF_Block_Block_Defn;

struct	EWF_Block_File;
typedef struct EWF_Block_File_Metadata{
	unsigned int	specVersion;
	unsigned int	fileVersion;
	unsigned int	createdTime;
	unsigned int	modifiedTime;
}EWF_Block_File_Metadata;
void* EWF_Block_File_Metadata_Parse(unsigned short,	//type
	unsigned char,									//options
	unsigned int,									//size
	EWF_MEMFILE*,									//stream
	unsigned int*,									//szOut
	void*);
void EWF_Block_File_Metadata_Write(unsigned short,		//type
	unsigned char,									//options
	void*,											//data
	unsigned int,									//size
	EWF_MEMFILE*,									//stream
	unsigned int*,									//szOut
	void*);	

typedef struct EWF_Block_File_Comment{
	char*			name;
	char*			comment;
}EWF_Block_File_Comment;
void* EWF_Block_File_Comment_Parse(unsigned short,	//type
	unsigned char,									//options
	unsigned int,									//size
	EWF_MEMFILE*,									//stream
	unsigned int*,									//szOut
	void*);
void EWF_Block_File_Comment_Write(unsigned short,		//type
	unsigned char,									//options
	void*,											//data
	unsigned int,									//size
	EWF_MEMFILE*,									//stream
	unsigned int*,									//szOut
	void*);	

typedef struct EWF_Block_Block{
	struct EWF_Block_File*	file;
	unsigned short	type;	//0x00 - 0xFF are reserved
	unsigned char	options;
	unsigned int	size;	//if this type has a parser, this is the size of the data returned by the parser
	void*			data;	//if this type has a parser, this is the result of that
}EWF_Block_Block;
EWF_Block_Block*	EWF_Block_Block_New		();
void				EWF_Block_Block_Free	(EWF_Block_Block* b);

typedef struct EWF_Block_File{
	EWF_MEMFILE*				stream;
	unsigned int				blockCount;
	EWF_LL*						blocks;
	EWF_BTree*					blockDefns;
	EWF_Block_File_Metadata*	metadata;
}EWF_Block_File;
EWF_Block_File*		EWF_Block_File_Open		(EWF_MEMFILE* mf);
int					EWF_Block_File_Parse	(EWF_Block_File* bf);
int					EWF_Block_File_Write	(EWF_Block_File* bf);
void				EWF_Block_File_Close	(EWF_Block_File* bf);
void				EWF_Block_File_Free		(EWF_Block_File* bf);
/*=======================*/
/*INI File Reading*/
/*Extensions to spec:
* -End of section delimiters:
*   A section header only containing the forward slash (i.e. [/])
*   denotes the end of the previous section.  This is most useful in
*   combination with section hierarchy (see below).
* -Section Hierarchy:
*   If this is enabled at parse-time, property names will have prefixes
*   based on the current active subsection.  For example, take the
*   following snippet:
*   - [Section1]
*   - property1=value1
*   - [subSection1]
*   - property2=value2
*   - [/]
*   - property3=value3
*   - [/]
*   - property4=value4
*   The properties here would be accessed using the following names
*   (respectively):
*   - Section1.property1
*   - Section1.subSection1.property2
*   - Section1.property3
*   - property4
*   If this is not enabled at parse-time, each property will be
*   accessible only by its name.
* -Lowercase Matching:
*   If this is enabled at parse-time, all properties will be
*   read in lowercase, and all lookups will be done in lowercase.
*   Values and sections will still preserve their case.
* -Directives
*   Start a comment with --directive to control parsing behavior
*   at parse time.  The following are accepted directives:
*   - #--directive hierarchy on
*     #--directive hierarchy off
*      turns on/off section hierarchy
*   - #--directive lowercase on
*     #--directive lowercase off
*      turns on/off lowercase matching
*/
/*Specific implementation behavior:
* -Leading and Trailing Whitespace is ignored
* -All values are stored as char* strings
* -Comments must start with # or ; and be on their own lines
* -Non-leading and non-trailing whitespace is preserved verbatim
* -Lines must be less than 256 characters long
*/
typedef struct struct_INI_File{
	EWF_MEMFILE*		stream;
	EWF_ClassicDict*	values;
	int				_lowercase;
}EWF_INI_File;
EWF_INI_File*	EWF_INI_Parse	(EWF_MEMFILE* stream,int hierarchy,int lowercase);
char*			EWF_INI_Get		(EWF_INI_File* ini,char* name);
void			EWF_INI_Free	(EWF_INI_File* ini,int closeFile);
/*=======================*/
#ifdef __cplusplus
}
#endif

#endif
