#include "ewfplat.h"

#include <stdlib.h>
#include <Windows.h>

void				EWFP_urandom		(void* buffer,unsigned int size){
	static HCRYPTPROV prov=(HCRYPTPROV)NULL;
	if(prov==(HCRYPTPROV)NULL){
		if(!CryptAcquireContextA(&prov,"ewfplat",NULL,PROV_RSA_FULL,0)){
			CryptAcquireContextA(&prov,"ewfplat",NULL,PROV_RSA_FULL,CRYPT_NEWKEYSET);
		}
	}
	CryptGenRandom(prov,size,buffer);
}
unsigned int		EWFP_GetTicks		(){
	static LARGE_INTEGER start;
	static LARGE_INTEGER tps={0};
	LARGE_INTEGER now;
	if(tps.LowPart==0){
		QueryPerformanceFrequency(&tps);
		QueryPerformanceCounter(&start);
	}
	QueryPerformanceCounter(&now);
	now.QuadPart-=start.QuadPart;
	now.QuadPart*=1000;
	now.QuadPart/=tps.QuadPart;
	return (DWORD)now.QuadPart;
}

unsigned int		EWFP_Sys_GetPageSize	(){
	SYSTEM_INFO si={0};
	GetSystemInfo(&si);
	return si.dwPageSize;
}

const int			EWFP_VALLOC_BIGPAGE		=0x00000001;
const int			EWFP_VALLOC_PAGE_x		=0x00010000;
const int			EWFP_VALLOC_PAGE_xr		=0x00020000;
const int			EWFP_VALLOC_PAGE_xrw	=0x00040000;
const int			EWFP_VALLOC_PAGE_r		=0x00080000;
const int			EWFP_VALLOC_PAGE_rw		=0x00100000;
const int			EWFP_VALLOC_PAGE_w		=0x00200000;
void*				EWFP_valloc			(void* start,size_t size,unsigned int flags){
	unsigned int sysflags=0;
	unsigned int prot=0;
	void* ret=NULL;
	if(flags & EWFP_VALLOC_PAGE_x)
		prot=PAGE_EXECUTE;
	if(flags & EWFP_VALLOC_PAGE_xr)
		prot=PAGE_EXECUTE_READ;
	if(flags & EWFP_VALLOC_PAGE_xrw)
		prot=PAGE_EXECUTE_READWRITE;
	if(flags & EWFP_VALLOC_PAGE_r)
		prot=PAGE_READONLY;
	if(flags & EWFP_VALLOC_PAGE_rw)
		prot=PAGE_READWRITE;
	if(flags & EWFP_VALLOC_PAGE_w)
		prot=PAGE_READWRITE;
	if(flags & EWFP_VALLOC_BIGPAGE)
		sysflags|=MEM_LARGE_PAGES;
	sysflags|=MEM_COMMIT|MEM_RESERVE;
	ret=VirtualAlloc(start,size,sysflags,prot);
	return ret;
}
void				EWFP_vfree			(void* chunk){
	VirtualFree(chunk,0,MEM_RELEASE);
}

struct EWFP_Mutex{
	CRITICAL_SECTION	cs;
};
EWFP_Mutex* EWFP_Mutex_New(){
	EWFP_Mutex* m=calloc(1,sizeof(*m));
	InitializeCriticalSection(&(m->cs));
	return m;
}
int EWFP_Mutex_Lock(EWFP_Mutex* mutex){
	EnterCriticalSection(&(mutex->cs));
	return 0;
}
int EWFP_Mutex_Unlock(EWFP_Mutex* mutex){
	LeaveCriticalSection(&(mutex->cs));
	return 0;
}
void EWFP_Mutex_Free(EWFP_Mutex* mutex){
	DeleteCriticalSection(&(mutex->cs));
	free(mutex);
}

struct EWFP_Condition{
	CONDITION_VARIABLE	cv;
};
EWFP_Condition* EWFP_Condition_New(){
	EWFP_Condition* c=calloc(1,sizeof(*c));
	InitializeConditionVariable(&(c->cv));
	return c;
}
int EWFP_Condition_Wait(EWFP_Condition* cond,EWFP_Mutex* mutex,unsigned int timeout){
	return SleepConditionVariableCS(&(cond->cv),&(mutex->cs),(timeout==0?INFINITE:timeout));
}
int EWFP_Condition_Signal(EWFP_Condition* cond){
	WakeConditionVariable(&(cond->cv));
	return 0;
}
int EWFP_Condition_Broadcast(EWFP_Condition* cond){
	WakeAllConditionVariable(&(cond->cv));
	return 0;
}
void EWFP_Condition_Free(EWFP_Condition* cond){
	//no DeleteConditionVariable?
	free(cond);
}

struct EWFP_Semaphore{
	HANDLE	semHandle;
};
EWFP_Semaphore* EWFP_Semaphore_New(unsigned int initialValue,unsigned int maxValue){
	EWFP_Semaphore* s=calloc(1,sizeof(*s));
	s->semHandle=CreateSemaphore(NULL,initialValue,maxValue,NULL);
	return s;
}
int EWFP_Semaphore_Post(EWFP_Semaphore* sem){
	return ReleaseSemaphore(sem->semHandle,1,NULL);
}
int EWFP_Semaphore_Wait(EWFP_Semaphore* sem,unsigned int timeout){
	return WaitForSingleObject(sem->semHandle,(timeout==0?INFINITE:timeout));
}
int EWFP_Semaphore_WaitNoBlock(EWFP_Semaphore* sem){
	return WaitForSingleObject(sem->semHandle,0);
}
void EWFP_Semaphore_Free(EWFP_Semaphore* sem){
	CloseHandle(sem->semHandle);
	free(sem);
}

typedef struct EWFP_WrapperData{
	int (*ep)(void*);
	void* closure;
	EWFP_Thread* thread;
}EWFP_WrapperData;
struct EWFP_Thread{
	HANDLE tHandle;
	EWFP_Thread_State	state;
	EWFP_WrapperData*	wd;
};
int		EWFP_Thread_Wrapper	(void* vdata){
	EWFP_WrapperData* wd=vdata;
	int ret=0;
	wd->thread->state=EWFP_THREAD_STATE_RUNNING;
	ret=wd->ep(wd->closure);
	wd->thread->state=EWFP_THREAD_STATE_TERMINATED;
	return ret;
}
EWFP_Thread* EWFP_Thread_New(int (*fxn)(void*),void* data){
	EWFP_Thread* t=calloc(1,sizeof(*t));
	EWFP_WrapperData* wd=calloc(1,sizeof(*wd));
	t->state=EWFP_THREAD_STATE_SUSPENDED;
	t->wd=wd;
	wd->ep=fxn;
	wd->closure=data;
	wd->thread=t;
	t->tHandle=CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)EWFP_Thread_Wrapper,wd,CREATE_SUSPENDED,NULL);
	return t;
}
int EWFP_Thread_Start(EWFP_Thread* thread){
	thread->state=EWFP_THREAD_STATE_RUNNING;
	return ResumeThread(thread->tHandle);
}
int EWFP_Thread_Kill(EWFP_Thread* thread){
	thread->state=EWFP_THREAD_STATE_TERMINATED;
	return TerminateThread(thread->tHandle,0);
}
int EWFP_Thread_Join(EWFP_Thread* thread){
	return WaitForSingleObject(thread->tHandle,INFINITE);
}
unsigned int EWFP_Thread_GetID(EWFP_Thread* thread){\
	return GetThreadId(thread->tHandle);
}
void EWFP_Thread_Free(EWFP_Thread* thread){
	EWFP_Thread_Kill(thread);
	free(thread->wd);
	free(thread);
}
void EWFP_Thread_Sleep(unsigned int ms){
	Sleep(ms);
}
EWFP_Thread_State		EWFP_Thread_GetState	(EWFP_Thread* thread){
	return thread->state;
}

struct EWFP_DSO{
	HMODULE hMod;
};
EWFP_DSO*	EWFP_DSO_Open	(const char* filename){
	HMODULE mod=LoadLibrary(filename);
	EWFP_DSO* ret=NULL;
	if(mod==NULL)
		return NULL;
	ret=calloc(1,sizeof(*ret));
	ret->hMod=mod;
	return ret;
}
void*		EWFP_DSO_GetAddress	(EWFP_DSO* dso,const char* name){
	FARPROC proc=NULL;
	if(!dso || !(dso->hMod))
		return NULL;
	proc=GetProcAddress(dso->hMod,name);
	return (void*)proc;
}
void		EWFP_DSO_Close	(EWFP_DSO* dso){
	if(dso && dso->hMod){
		CloseHandle(dso->hMod);
		free(dso);
	}
}